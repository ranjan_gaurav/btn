window.onload = function () {
    CanvasJS.addColorSet("greenShades",
            [
                "#1f6aa4",
                "#8abcdc",
                "#cee9fa"
            ]);

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        animationDuration: 2000, //change to 1000, 500 etc
        //height: 270,
        colorSet: "greenShades",
        axisY: {
            labelFontSize: 12
        },
        axisX: {
            labelFontSize: 12
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                    {label: "Jan", y: 25},
                    {label: "Feb", y: 20},
                    {label: "Mar", y: 20},
                ]
            }
        ]
    });
    chart.render();
    CanvasJS.addColorSet("greenShades1",
            [
                "#1f6aa4",
                "#8abcdc",
                "#cee9fa"
            ]);
    var chart1 = new CanvasJS.Chart("chartContainer1", {
        animationEnabled: true,
        animationDuration: 1500, //change to 1000, 500 etc
        //width: 160,
        height: 100,
        colorSet: "greenShades1",
        axisY: {
            labelFontSize: 11
        },
        axisX: {
            labelFontSize: 11
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                    {label: "Jan", y: 8},
                    {label: "Feb", y: 5},
                    {label: "Mar", y: 5},
                ]
            },
		],
		
    });
    chart1.render();
    CanvasJS.addColorSet("greenShades2",
            [
                "#cee9fa",
                "#cee9fa",
                "#1f6aa4"
            ]);
    var chart2 = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        animationDuration: 1000, //change to 1000, 500 etc
        //width: 160,
        height: 100,
        colorSet: "greenShades2",
        axisY: {
            labelFontSize: 11
        },
        axisX: {
            labelFontSize: 11
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                    {label: "Jan", y: 8},
                    {label: "Feb", y: 5},
                    {label: "Mar", y: 7},
                ]
            }
        ]
    });
    chart2.render();
    CanvasJS.addColorSet("greenShades3",
            [
                "#1f6aa4",
                "#1f6aa4",
                "#cee9fa"
            ]);
    var chart3 = new CanvasJS.Chart("chartContainer3", {
        animationEnabled: true,
        animationDuration: 800, //change to 1000, 500 etc
        //width: 160,
        height: 100,
        colorSet: "greenShades3",
        axisY: {
            labelFontSize: 11
        },
        axisX: {
            labelFontSize: 11
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                    {label: "Jan", y: 8},
                    {label: "Feb", y: 5},
                    {label: "Mar", y: 5},
                ]
            }
        ]
    });
    chart3.render();
    CanvasJS.addColorSet("greenShades4",
            [
                "#cee9fa",
                "#8abcdc",
                "#1f6aa4",
            ]);
    var chart4 = new CanvasJS.Chart("chartContainer4", {
        animationEnabled: true,
        animationDuration: 500, //change to 1000, 500 etc
        //width: 160,
        height: 100,
        colorSet: "greenShades4",
        axisY: {
            labelFontSize: 11
        },
        axisX: {
            labelFontSize: 11
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                    {label: "Jan", y: 8},
                    {label: "Feb", y: 5},
                    {label: "Mar", y: 5},
                ]
            }
        ]
    });
    chart4.render();
}
$(document).ready(function () {
$('.data_decade_view1 .input-group.date').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
});
});