<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Opportunity Contest Dashboard - Online Banking</h2></div>
</div>
<div class="row">
    <div class="top-space">
	<div class="col-lg-12 col-sm-12 col-md-12">
<div class="user-name1"><b>Customer <?php echo $client->name; ?></b></div>
</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-content">
						<!--<div class="user-name"><b>MINGGU SWANTO</b></div>-->
                            <strong>Customer is NOT subscribed to internet banking</strong><br/>
                            <strong>Over the past 3 months, customer has performed the following transactions, which can be done online:</strong>
                            <ul class="todo-list m-t ui-sortable">
                                <li>                                        
                                    <p class="text-center"><button type="button" class="btn btn-w-m btn-white pull-left font-bold">16</button> Wire Transfers</p>
                                </li>
                                <li>                                        
                                    <p class="text-center"><button type="button" class="btn btn-w-m btn-white pull-left font-bold">15</button> Cheques Issued</p>
                                </li>
                                <li>                                        
                                    <p class="text-center"><button type="button" class="btn btn-w-m btn-white pull-left font-bold">6</button> Bill Payments</p>
                                </li>
                                <li>                                        
                                    <p class="text-center"><button type="button" class="btn btn-w-m btn-white pull-left font-bold">7</button> Other transactions that can be done online</p>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>

            </div>


        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">

        <a href="<?php echo base_url('rrm/profile');?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp; &nbsp; Go Back</a>

    </div>
</div>
