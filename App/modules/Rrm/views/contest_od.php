<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Opportunity Contest Dashboard – OD Utilization</h2></div>
</div>
<div class="row"><div class="top-space">
<div class="col-lg-12 col-sm-12 col-md-12">
<div class="user-name1"><b>Customer <?php echo $client->name; ?></b></div>
</div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5>Utilization (%)</h5>               
                </div>
                <div class="ibox-content">
				<div class="">
				<div class="graph55">
				<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Utilization.svg" class="img-t" alt="financial_kpi">
				</div>
				</div>
                    <!-- <div id="morris-one-line-chart"></div>-->
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">

        <a href="<?php echo base_url('rrm/profile');?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp; &nbsp; Go Back</a>

    </div>
</div>