<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2 class="heading-gap">RRM Leader Performance Dashboard – Team view</h2></div>
	<div class="col-lg-2"><a class="btn btn-w-m btn-info pull-right" href="<?php echo base_url('home');?>">Expansion</a></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-9 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content hun-performance">
				                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6 col-sm-4 col-lg-4">
                                    <div class="form-group" id="data_decade_view1">
                                        <div class="input-group date">
                                            <span class="input-group-addon">Start Date</span><input type="text" class="form-control" value="03/04/2014">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-4 col-lg-4">
                                    <div class="form-group" id="data_decade_view1">
                                        <div class="input-group date">
                                            <span class="input-group-addon">End Date</span><input type="text" class="form-control" value="03/04/2014">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <div id="content-md" class="content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Financial KPI</th>
                                        <th class="text-center" colspan="4">Incentive scheme</th>

                                        <th class="text-center" colspan="5">Process indicator</th>

                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>

                                    </tr>
                                    <tr>
                                        <th rowspan="">Hunter Name </th>
                                        <th># NTB</th>
                                        <th># NTB w/t bundle</th>
                                        <th># NTB w/t loan only</th>
                                        <th># NTB w/o loan</th>
                                        <th>Successful Referrals</th>
                                        <th>Leads created</th>
                                        <th>Leads conversion</th>
                                        <th># Visits</th>
                                        <th># Visits per Lead</th>
                                        <th>App. approval rate</th>
                                        <th>TAT b/t created to sub.</th>
                                        <th>TAT b/t sub. to onboard</th>
                                        <th>Relative Performance</th>

                                    </tr>

                                </thead>
                                <tbody>
                                    <?php foreach ($result as $list) : ?>
                                        <tr class="gradeX">

                                            <td><?php echo $list->hunter_name; ?></td>
                                            <td><?php echo $list->sum_ntb; ?></td>
                                            <td><?php echo $list->sum_ntb_with_bundle; ?></td>
                                            <td><?php echo $list->sum_ntb_with_loan_only; ?></td>
                                            <td><?php echo $list->sum_ntb_without_loan; ?></td>
                                            <td><?php echo $list->sum_successful_refererrals; ?></td>
                                            <td><?php echo $list->sum_lead_creation; ?></td>
                                            <td><?php echo $list->sum_lead_conversion; ?></td>
                                            <td><?php echo $list->sum_visits; ?></td>
                                            <td><?php echo $list->sum_visit_per_lead; ?></td>
                                            <td><?php echo $list->sum_approx_approval_rate; ?></td>
                                            <td><?php echo $list->sum_tat_bt_created_to_sub; ?></td>
                                            <td><?php echo $list->sum_tat_bt_sub_to_onboard; ?></td>
                                            <td>200</td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-3 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="4">Alerts</th>

                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Escalated to</th>
                                    <th>Delete</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr class="gradeX">
                                    <td>Process</td>
                                    <td>6mnths hunter % approv</td>
                                    <td>Area manager</td>
                                    <td class="center">
                                        <a class="btn btn-white btn-bitbucket">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>
                                <tr class="gradeC">
                                    <td>Process</td>
                                    <td>6mnths hunter % approv</td>
                                    <td>Area manager</td>
                                    <td class="center">
                                        <a class="btn btn-white btn-bitbucket">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>
                                <tr class="gradeA">
                                    <td>Process</td>
                                    <td>6mnths hunter % approv</td>
                                    <td>Area manager</td>
                                    <td class="center">
                                        <a class="btn btn-white btn-bitbucket">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>
                                <tr class="gradeA">
                                    <td>Process</td>
                                    <td>6mnths hunter % approv</td>
                                    <td>Area manager</td>
                                    <td class="center">
                                        <a class="btn btn-white btn-bitbucket">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>

                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
