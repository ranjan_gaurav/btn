<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Performance Dashboard</h2></div>
</div>
<?php
$hunterInfo = getUserInfo(@$this->session->userdata('userid'));
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <div class="view-btn">
                <div class="form-group">
				<div class="view-name">View :<span><?php echo $hunterInfo->first_name; ?></span></div>
                </div>
                            </div>
                            <div class="profile-image">
                                <img src="<?php echo base_url(); ?>/assets/img/<?php echo $hunterInfo->user_pic; ?>" class="img-circle circle-border m-b-md" alt="profile">
                            </div>
                            <div class="profile-info">
                                <div class="">
                                    <div>
                                        <h2 class="no-margins">
                                            <?php echo $hunterInfo->first_name; ?>
                                        </h2>
                                        <h4>+62 2345 2345</h4>
                                        <h4><?php echo $hunterInfo->username; ?>@btn.co.id</h4>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="information-section">
                                    <div class="heading-l">Last Month Performance </div>
                                    <div class="information-section-area">
                                        <div class="information-section-content">Incentive Score</div>
                                        <div class="information-section-content1">30</div>
                                    </div>
                                    <div class="information-section-area">
                                        <div class="information-section-content">Ranking (Percentile)</div>
                                        <div class="information-section-content1">15%</div>
                                    </div>

                                    <div class="information-section-area">
                                        <div class="information-section-content">Bonus Earned (*)(IDR '000)</div>
                                        <div class="information-section-content1">1,000</div>
                                    </div>
                                </div>

                            </div>

                            </div>
                            <?php
                            $array = array();
                            foreach ($result as $arrdata) :
                                array_push($array, $arrdata->ntb);
                            endforeach;
                            ?>
                            <script>
                                var financialntb = [];
                                var ntbwithloadbundle = [];
                                var ntbwithloan = [];
                                var ntbwithoutloan = [];
                                var successfullrefferals = [];
                                var months = [];
<?php foreach ($result as $data) : ?>
                                    financialntb.push(['<?php echo $data->ntb ?>']);
                                    ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
                                    ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
                                    ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
                                    successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
                                    months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
                            </script>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 chart-padding-middle">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Financial and Risk Management KPIs.</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Loan Balance Growth</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="loanbunddlechart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Loan_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Term Loan Re-Instatement</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Term_Loan_Re-instatement.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>CASA Balance Growth </h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/CASA_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Average CASA Transactions/Customer</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Average_CASA_Transactions-customer.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Portfolio NPL</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Portfolio_NPL.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Cross selling points</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Cross_selling_points.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="col-lg-6 col-sm-6 col-md-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Non-Financial KPIs</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="financial_kpi">
                                                                <h5>Risk Management</h5>
                                                                <div class="risk-management">
                                                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Riskmanagement.png" class="img-t" alt="Risk Management">
                                                                </div>
                                                                <div class="line"></div>
                                                 <div id="chartContainer"></div>
                                 <canvas id="ntbChart" height="160"></canvas> 
                                <div class="legends">
                                    <div class="target"><i class="fa fa-square"></i> Target</div>
                                    <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                            </div>
                            </div>

