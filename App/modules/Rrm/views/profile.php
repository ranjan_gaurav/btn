<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9 col-sm-9 col-md-9">
        <h2 class="head-up2">Customer Profile</h2>
        <div class="select-items">
            <select name="Company_Name" class="selectpicker required" id="Company_ID" onChange="showUser(this.value)">
                <option value="4" <?php
                if ($client_id == '4') {
                    echo "selected";
                }
                ?>>Customer A</option>
                <option value="5" <?php
                if ($client_id == '5') {
                    echo "selected";
                }
                ?>>Customer B</option>
                <option value="7" <?php
                if ($client_id == '7') {
                    echo "selected";
                }
                ?>>Customer C</option>
                <option value="3" <?php
                if ($client_id == '3') {
                    echo "selected";
                }
                ?>>Customer D</option>
                <option value="2" <?php
                if ($client_id == '2') {
                    echo "selected";
                }
                ?>>Customer E</option>

            </select>

        </div>
        <form method="post" action="<?php echo base_url('referrals'); ?>">
            <input type="hidden" name="client_name" value="<?php echo $client->name; ?>">
            <button class="btn btn-w-m btn-primary indi-refer" type="submit">Refer</button>
        </form>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3 padding-r-lt">
        <!--<div class="col-lg-5 col-sm-5 col-md-5 padding-r">
            <div class="input-group m-b-1"><input type="text" class="form-control" placeholder="Search"> <span class="input-group-addon"><i class="fa fa-search"></i></span></div>
        </div>
        <div class="col-lg-7 col-sm-7 col-md-7 padding-r date-up">-->
        <div class="form-group" id="data_decade_view12">
            <div class="input-group date">
                <span class="input-group-addon">Select Date</span><input type="text" class="form-control" value="04/04/2016" disabled>
            </div>
        </div>

    </div>
</div>
<div class="row animated fadeInRight">
    <div class="top-space">
        <div class="col-sm-4 col-md-4 mini-sp">
            <div class="ibox float-e-margins">
                <div class="ibox-title" id= "client_name">
                    <h5>Customer <?php echo $client->name; ?></h5>
                </div>
                <div class="ibox-content table-r">
                    <div class="content" id="content-md1">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="heading-p">Company information</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Industry:</th>
                                        <td><?php echo $client->industry; ?></td>

                                    </tr>


                                    <tr>
                                        <th>Sales (Bn)</th>
                                        <td><?php echo $client->sales; ?> Bn</td>

                                    </tr>
                                    <tr>
                                        <th>Assets (Bn)</th>
                                        <td><?php echo $client->assets; ?> Bn</td>

                                    </tr>

                                    <tr>
                                        <th>Grade</th>
                                        <td><?php echo $client->credit_rating; ?></td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ibox float-e-margins margins_rrm">
                <div class="ibox-title">
                    <h5>Assess opportunity context</h5>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <div class="lead-btns-tow">
                            <a href="<?php echo base_url('rrm/contest_casa/' . $client_id); ?>" class="btn btn-w-m btn-warning">CASA Account</a>
                            <a href="<?php echo base_url('rrm/contest_online/' . $client_id); ?>" class="btn btn-w-m btn-warning">Online Banking</a>
                            <a href="<?php echo base_url('rrm/contest_od/' . $client_id); ?>" class="btn btn-w-m btn-warning">Overdraft Utilization</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-sm-8 col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Metrics</h5>

                </div>
                <div class="ibox-content rrm_height">

                    <div class="table-responsive">
                        <div class="heading-p">Loans</div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Acct.</th>
                                    <th>Interest</th>
                                    <th>Balance (Mn)</th>
                                    <th>Limit (Mn)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>OD</td>
                                    <td>234212</td>
                                    <td>12%</td>
                                    <td colspan="2"><div class="progress progress-small progress-filled">500
                                            <div style="width: 80%;" class="progress-bar">1,500</div>
                                        </div></td>
                                    <!--<td><div class="progress progress-small progress-filled">
                                            <div style="width: 90%;" class="progress-bar"></div>
                                        </div></td>
                                    <td>ABF (IDR Bn)<div class="progress progress-small progress-filled">
                                            <div style="width: 20%;" class="progress-bar"></div>
                                        </div></td>-->

                                </tr>
                                <tr>
                                    <td>TL</td>
                                    <td>211232</td>
                                    <td>14%</td>
                                    <td colspan="2"><div class="progress progress-small progress-filled">
                                            <div style="width:100%;" class="progress-bar">2,000</div>
                                        </div></td>
                                    <!--<td><div class="progress progress-small progress-filled">
                                            <div style="width: 80%;" class="progress-bar"></div>
                                        </div></td>
                                    <td>ABF (IDR Bn)<div class="progress progress-small progress-filled">
                                            <div style="width: 20%;" class="progress-bar"></div>
                                        </div></td>-->

                                </tr>
                                <tr>
                                    <td>BPF</td>
                                    <td>233212</td>
                                    <td>9%</td>
                                    <td colspan="2"><div class="progress progress-small progress-filled">500
                                            <div style="width: 80%;" class="progress-bar">1,500</div>
                                        </div></td>
                                   <!-- <td><div class="progress progress-small progress-filled">
                                            <div style="width: 80%;" class="progress-bar"></div>
                                        </div></td>
                                    <td>ABF (IDR Bn)<div class="progress progress-small progress-filled">
                                            <div style="width: 20%;" class="progress-bar"></div>
                                        </div></td>-->

                                </tr>
                            </tbody>
                        </table>
                        <div class="heading-p">Deposits</div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Acct.</th>
                                    <th>Interest</th>
                                    <th>Balance (Mn)</th>
                                    <th><!--Limit (Mn)--></th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CA</td>
                                    <td>2278378</td>
                                    <td>0.5%</td>

                                    <td colspan="2"><div class="progress progress-small progress-filled">
                                            <div style="width: 100%;" class="progress-bar">2000</div>
                                        </div></td>

                                </tr>
                                <tr>
                                    <td>SA</td>
                                    <td>111111</td>
                                    <td>4%</td>

                                    <td colspan="2"><div class="progress progress-small progress-filled">
                                            <div style="width: 100%;" class="progress-bar">2,000</div>
                                        </div></td>

                                </tr>
                                <tr>
                                    <td>TD</td>
                                    <td>332242</td>
                                    <td>8%</td>

                                    <td colspan="2"><div class="progress progress-small progress-filled">
                                            <div style="width:100%;" class="progress-bar">2,000</div>
                                        </div></td>

                                </tr>
                            </tbody>
                        </table>
                    </div> 

                </div>

            </div>
        </div>


    </div>                  
</div>



