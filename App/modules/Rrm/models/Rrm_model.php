<?php

/*

 */

class Rrm_model extends CI_Model {

    var $table = "hunter";
    var $data_table = "hunter_data";
    var $client_table = "client_portfolio";
    var $activity_table = "activity";
    var $alert_table = "alerts";
    var $company_table = "company";
   
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }
    
    /*
     * Get Hunder information from the hunter table
     */

    public function getHunterInfo() {
        $Id = '3';
        $this->db->where('h.hunter_id', $Id);
        $this->db->select('h.*,SUM(`d`.`lead_creation`) as sum_lead_creation,SUM(`d`.`lead_conversion`) as sum_lead_conversion,SUM(`d`.`visits`) as sum_visits,SUM(`d`.`visit_per_lead`) as sum_visit_per_lead,SUM(`d`.`approx_approval_rate`) as sum_approx_approval_rate,SUM(`d`.`tat_bt_created_to_sub`) as sum_tat_bt_created_to_sub,SUM(`d`.`tat_bt_sub_to_onboard`) as sum_tat_bt_sub_to_onboard');
        $this->db->from($this->table . ' h');
        $this->db->group_by('h.hunter_id');
        $this->db->join($this->data_table . ' d', 'd.hunter_id=h.hunter_id');
        $query = $this->db->get();
        return $query->row();
    }

    /*
     * Get Hunder Data based on Hunter Id and Month Basis from the hunter_data table
     */

    public function getHunterData() {
        $Id = '1';
        $this->db->where('hunter_id', $Id);
        $this->db->group_by('MONTH(creation_date)');
        $query = $this->db->get($this->data_table);
        return $query->result();
    }

    public function getHunterList() {
        $this->db->select('h.*,SUM(`d`.`ntb`) as sum_ntb,SUM(`d`.`ntb_with_bundle`) as sum_ntb_with_bundle,SUM(`d`.`ntb_with_loan_only`) as sum_ntb_with_loan_only,SUM(`d`.`ntb_without_loan`) as sum_ntb_without_loan,SUM(`d`.`successful_refererrals`) as sum_successful_refererrals,SUM(`d`.`lead_creation`) as sum_lead_creation,SUM(`d`.`lead_conversion`) as sum_lead_conversion,SUM(`d`.`visits`) as sum_visits,SUM(`d`.`visit_per_lead`) as sum_visit_per_lead,SUM(`d`.`approx_approval_rate`) as sum_approx_approval_rate,SUM(`d`.`tat_bt_created_to_sub`) as sum_tat_bt_created_to_sub,SUM(`d`.`tat_bt_sub_to_onboard`) as sum_tat_bt_sub_to_onboard');
        $this->db->from($this->table . ' h');
        $this->db->group_by('h.hunter_id');
        $this->db->join($this->data_table . ' d', 'd.hunter_id=h.hunter_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getClientList(){
        $this->db->where_in('client_id',array('4','5','7'));
        $this->db->from($this->client_table);
        $this->db->limit(3);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getClientInfo($id){
    	
    	//$q = intval ( $_GET ['q'] );
    	
    	
        $this->db->where('client_id',$id);
        $this->db->from($this->client_table);
        $query=$this->db->get();
        return $query->row();
        
        
    }
    
    public function listActivity(){
        $this->db->from($this->activity_table);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getAlerts(){
        $this->db->from($this->alert_table);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function insertActivity() {
    	$data['company'] = $this->input->post('company');
    	$data['name'] = $this->input->post('name');
    	$data['purpose'] = $this->input->post('purpose');
    	$data['start_date'] = $this->input->post('start_date');
    	$data['end_date'] = $this->input->post('end_date');
        $data['description'] = $this->input->post('description');
        $query = $this->db->insert($this->activity_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getActivityDetails($id) {
        $this->db->where('activity_id',$id);
        $this->db->from($this->activity_table);
        $query = $this->db->get();
        return $query->row();
    }

}

?>