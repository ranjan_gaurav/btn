<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Sales extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('sales_model', 'sales');
        error_reporting(0);
    }

    public function index() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/sales.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Sales";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    public function bundle() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/sales.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Sales Bundle Selector";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    public function product() {
      
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/custom-js-for-product.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/iCheck/custom.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Sales Product Selector";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    public function product_select() {
    	
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/bootbox.js',
            'assets/js/sales.js',
        	'assets/js/max-min.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('document_name','Name','trim|required');
        if($this->form_validation->run()==false){
            $data['error']=validation_errors();
        }else{
            $this->sales->insertDocumentDetails();
        }
        $data['documents']=$this->sales->documentsList();
        $data['products']=$this->sales->GetProductsListing();
       
        $data['additional_product']=$this->sales->GetAdditionalProductsListing();
        $data['totals']=$this->sales->GetProductsDetailsTotal();
        $data['meta_title'] = "Bank BRI Sales Product Selector";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

}

?>