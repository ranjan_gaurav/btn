<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product Selector</h2></div>
</div>
<?php
//print_r($this->input->post());
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-md-5">
                        <div class="form-group star-check">
                            <div class="star"><i class="fa fa-star" aria-hidden="true"></i></div>
                            <input type="checkbox" value="" name="" disabled="disabled" class="i-checks"/>
                            <label>Recommended</label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-md-5">
                        <div class="form-group star-check">
                            <input type="checkbox" value="" name="" class="i-checks" checked disabled/>
                            <label>Mandatory Part of Bundle Selected</label>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <form method="post" action="<?php echo base_url('sales/product_select'); ?>">
              <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox product-s">

                                    <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
                                        <tbody>
                                            <tr class="footable-even">
                                                <td class="footable-visible footable-first-column">
                                                    <div class="style1 yellow-bg">
                                                        <div class="row vertical-align">
                                                            <div class="col-md-12">
                                                                <h4 class="font-bold">CASA</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="2" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on' || $this->input->post('wc-bundle') == 'on' || $this->input->post('casa') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <div class="star1"><?php if ($this->input->post('casa')!='on' && !$this->input->post('wc-bundle') && !$this->input->post('mortgage-bundle') ) { ?><i class="fa fa-star" aria-hidden="true"></i><?php } ?></div>
                                                                <h3>Giro</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="20" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('wc-bundle') == 'on' || $this->input->post('mortgage-bundle') == 'on' || $this->input->post('casa') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <div class="star1"></div>
                                                                <h3>Bilyet Giro / Buku Cek</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="23" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('casa') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <div class="star1"></div>
                                                                <h3>abc</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12 star-check">
                                                                <input type="checkbox" value="13" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('casa') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <div class="star1"><?php if (!$this->input->post('casa') && $this->input->post('wc-bundle')=='on' && $this->input->post('mortgage-bundle')=='on') { ?><i class="fa fa-star" aria-hidden="true"></i><?php } ?></div>
                                                                <h3>Flexibal</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <!--<td class="footable-visible">
                                                    <div class="style1 yellow-bg">
                                                        <div class="row vertical-align">
                                                            <div class="col-md-12">
                                                                <h3 class="font-bold">Value Added Services</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>-->
                                                
                                            </tr>
                                            <tr class="footable-even">
                                                <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>
                                                    <div class="style1 yellow-bg">
                                                        <div class="row vertical-align">
                                                            <div class="col-md-12">
                                                                <h4 class="font-bold">Loan</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="1" name="sub_products1[]" class="i-checks" <?php
                                                                if ($this->input->post('wc-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <div class="star1"><?php if ($this->input->post('casa')=='on' || !$this->input->post('wc-bundle') || !$this->input->post('mortgage-bundle') ) { ?><i class="fa fa-star" aria-hidden="true"></i><?php } ?></div>
                                                                <h3>KRK / KAB </h3>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>

                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="8" name="sub_products1[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>KTU</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="26" name="sub_products1[]" class="i-checks"/>
                                                                <h3>KB</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="27" name="sub_products1[]" class="i-checks"/>
                                                                <h3>KB Bisa</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <!--<td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>-->
                                                
                                            </tr>
                                            <tr class="footable-even">
                                                <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>
                                                    <div class="style1 yellow-bg">
                                                        <div class="row vertical-align">
                                                            <div class="col-md-12">

                                                                <h4 class="font-bold">Transaction <br/>Solution</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="5" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on' || $this->input->post('wc-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>Business card</h3>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>

                                                
                                                <td class="footable-visible ">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12 star-check">
                                                            <input type="checkbox" value="10" name="sub_products[]" class="i-checks" <?php
                                                            if ($this->input->post('b2b') == 'on') {
                                                                echo "checked disabled";
                                                            }
                                                            ?>/>
                                                            <h3>Business Debit card</h3>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="15" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on' || $this->input->post('casa') == 'on' || $this->input->post('wc-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>E-Channel</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>
                                                
                                                
                                            </tr>
                                            
                                            <tr  class="footable-even">
                                                <td class="footable-visible footable-first-column"><span class="footable-toggle"></span>
                                                    <div class="style1 yellow-bg">
                                                        <div class="row vertical-align">
                                                            <div class="col-md-12">

                                                                <h4 class="font-bold">Personal <br/>Products </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="footable-visible">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="Insurance" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>Insurance</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td class="footable-visible">
                                                   <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="24" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('mortgage-bundle') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>Personal Credit Card</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible ">
                                                    <div class="style1 grey-bg">
                                                        <div class="row vertical-align h3_checkbox">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" value="21" name="sub_products[]" class="i-checks" <?php
                                                                if ($this->input->post('wc-bundle') == 'on'  || $this->input->post('mortgage-bundle') == 'on' || $this->input->post('casa') == 'on') {
                                                                    echo "checked disabled";
                                                                }
                                                                ?>/>
                                                                <h3>Personal Debit Card</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="footable-visible">
                                                    <div class="width-d">&nbsp;</div>
                                                </td>
                                                
                                                
                                                
                                            </tr>
                                        </tbody>

                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="lead-btns">
                        <?php
                        $mainIds = array();
                        if ($this->input->post('wc-bundle') == 'on') {
                            $mainIds[] = '1';
                        }
                        if ($this->input->post('mortgage-bundle') == 'on') {
                            $mainIds[] = '2';
                        }
                        if ($this->input->post('casa') == 'on') {
                            $mainIds[] = '3';
                        }
                        $addIds = array();
                        if ($this->input->post('b2b') == 'on') {
                            $addIds[] = '4';
                        }
                        if ($this->input->post('restaurant') == 'on') {
                            $addIds[] = '5';
                        }
                        if ($this->input->post('online-toolkit') == 'on') {
                            $addIds[] = '6';
                        }
                        ?>
                        <input type="hidden" name="main_product_ids" value="<?php echo implode(',', $mainIds); ?>">
                        <input type="hidden" name="additional_ids" value="<?php echo implode(',', $addIds); ?>">
                        <a href="<?php echo base_url('sales/bundle'); ?>" class="btn btn-w-m btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        <button type="submit" class="btn btn-w-m btn-primary submit-product" type="button" disabled>Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    });
</script>	