<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Product Selector</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <form method="post" action="<?php echo base_url('sales/product'); ?>">
        <div class="row">
            <div class="col-lg-12 space-rem loan">
                <div class="ibox-content">
                    <div class="space-t-b">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-4 col-sm-6 col-md-6 mini-select">
                                    <label>Industry</label>
                                    <select class="selectpicker industryPicker" name="industry">
                                        <option value="select">Select</option>
                                        <option value="fb">F&B</option>
                                        <option value="fmcg">FMCG</option>
                                        <option value="construction">Construction</option>
                                        <option value="autoparts">Autoparts</option>
                                        <option value="restaurant">Restaurant</option>
                                        <option value="retailers">Retailers</option>
                                        <option value="fashion">Fashion</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-sm-6 col-md-6 mini-select">
                                    <label>Loan Needed</label>
                                    <select class="selectpicker loanPicker" name="loan_needed">
                                        <option value="select">Select</option>
                                        <option value="facility">Yes, for working capital</option>
                                        <option value="bizz">Yes, for business premises</option>
                                        <option value="casa">No, interested in CASA only</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="carousel slide" id="carousel3">
                        <div class="col-sm-6 col-md-6"><h3>Select your BUNDLE(s):</h3></div>
                        <div class="carousel-inner">
                            <div class="item gallery active">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4 col-sm-4 col-md-4">
                                            <div class="contact-box bundle-padding center-version light-grey-bg img-view">

                                                <h3 class="m-b-xs with-radio-btn"><strong>Bundle 1</strong>
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox11" type="checkbox" class="facility enable-button" name="wc-bundle">
                                                        <label for="checkbox11">
                                                        </label>
                                                    </div>
                                                </h3>
                                                <ul class="bundles">
                                                    <li>KRK / KAB</li>
                                                    <li>Business Card</li>
                                                    <li>Personal Debit Card</li>
                                                    <li>Bilyet Giro / Buku Cek</li>
                                                    <li>E-Channel</li>
                                                </ul>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/Laporan Tahunan PT Bank Permata Tbk 2016.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>

                                                </div>

                                                <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/odbunddle.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">
                                            <div class="contact-box bundle-padding center-version light-grey-bg img-view">

                                                <h3 class="m-b-xs with-radio-btn"><strong>Bundle 2</strong><div class="checkbox checkbox-primary">
                                                        <input id="checkbox12" type="checkbox" class="bizz enable-button" name="mortgage-bundle">
                                                        <label for="checkbox12">

                                                        </label>
                                                    </div></h3>
                                                <ul class="bundles">
                                                    <li>KTU</li>
                                                    <li>Business Card</li>
                                                    <li>Personal Debit Card</li>
                                                    <li>Bilyet Giro / Buku Cek</li>
                                                    <li>E-channel</li>
                                                </ul>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>

                                                </div>

                                                <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/mortgagebundle.png" alt=""></div>
                                            </div>

                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">
                                            <div class="contact-box bundle-padding center-version light-grey-bg img-view">

                                                <h3 class="m-b-xs with-radio-btn"><strong>Bundle 3</strong><div class="checkbox checkbox-primary">
                                                        <input id="checkbox13" type="checkbox" class="casa enable-button" name="casa">
                                                        <label for="checkbox13">

                                                        </label>
                                                    </div></h3>
                                                <ul class="bundles">
                                                    <li>Giro</li>
                                                    <li>Special TD</li>
                                                    <li>High Interest Tabungan</li>
                                                    <li>Personal Debit Card</li>
                                                    <li>Bilyet Giro / Buku Cek</li>
                                                    <li>E-Channel</li>
                                                </ul>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>
                                                </div>
                                                <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/casabundle.png" alt=""></div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <a data-slide="prev" href="#carousel3" class="left carousel-control">
                            <span class="icon-prev"></span>
                        </a>
                        <a data-slide="next" href="#carousel3" class="right carousel-control">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                </div>

            </div>
            <div class="col-md-12 space-rem industry">
                <div class="ibox-content">
                    <div class="col-lg-12"><h3>Select Your VAS Solution(s):</h3></div>
                    <div class="carousel slide" id="carousel4">
                        <div class="carousel-inner">
                            <div class="item gallery active">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4 col-sm-4 col-md-4">
                                            <div class="contact-box bundle-padding center-version light-grey-bg">
											<div class="with-radio-btn1">
                                                        <div class="checkbox checkbox-primary">
                                                            <input id="checkbox4" type="checkbox" class="all-checkbox fb fashion autoparts construction fmcg" name="b2b">
                                                            <label for="checkbox4"></label>
                                                        </div>
                                                    </div>
                                                <div class="row">
                                                    
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/networking.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 1:</strong></h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">

                                            <div class="contact-box bundle-padding center-version light-grey-bg">
                                                <div class="with-radio-btn1">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox5" type="checkbox" class="all-checkbox restaurant" name="restaurant">
                                                        <label for="checkbox5">

                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/point-of-sale.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 2:
                                                            </strong>
                                                        </h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">

                                            <div class="contact-box bundle-padding center-version light-grey-bg">
                                                <div class="with-radio-btn1">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox6" type="checkbox" class="all-checkbox retailers" name="online-toolkit">
                                                        <label for="checkbox6">

                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/go-online3.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 3:</strong></h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="item gallery">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4 col-sm-4 col-md-4">
                                            <div class="contact-box bundle-padding center-version light-grey-bg">
											<div class="with-radio-btn1">
                                                        <div class="checkbox checkbox-primary">
                                                            <input id="checkbox7" type="checkbox">
                                                            <label for="checkbox7"></label>
                                                        </div>
                                                    </div>
                                                <div class="row">
                                                    
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/networking.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 4:</strong></h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">

                                            <div class="contact-box bundle-padding center-version light-grey-bg">
                                                <div class="with-radio-btn1">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox8" type="checkbox">
                                                        <label for="checkbox8">

                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/point-of-sale.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 5:</strong>
                                                        </h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4">

                                            <div class="contact-box bundle-padding center-version light-grey-bg">
                                                <div class="with-radio-btn1">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox9" type="checkbox">
                                                        <label for="checkbox9">

                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="image-box"><img src="<?php echo base_url(); ?>assets/img/go-online3.png" alt=""></div>
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                                                        <h3 class="m-b-xs with-radio-btn text-headings one"><strong>VAS 6:</strong></h3>
                                                    </div>

                                                </div>
                                                <div class="m-t-xs btn-group">
                                                    <a class="btn btn-xs btn-white" href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download  </a>
                                                    <a class="btn btn-xs btn-white" data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle" aria-hidden="true"></i> Play</a>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <a data-slide="prev" href="#carousel4" class="left carousel-control">
                            <span class="icon-prev"></span>
                        </a>
                        <a data-slide="next" href="#carousel4" class="right carousel-control">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                </div>
                    <div class="row"><div class=" col-lg-12 lead-btns">
                            <button type="submit" class="btn btn-primary btn-w-m submit-product" >Next</button>
                        </div></div>
            </div>
        </div>
    </form>
</div>
<div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Dmobile</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/oESHamqLhkQ" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>
</div>