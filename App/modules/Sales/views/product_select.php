<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product Selection Summary</h2></div>
</div>
<?php
//print_r($this->input->post());
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-7 col-sm-7 col-md-7">
                    <div class="ibox float-e-margins22">
                        <div class="ibox-title"><h5>Products &amp; Services Selected</h5></div>
                        <div class="ibox-content content-table1 content-table">
                            <div id="content-md" class="content">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr class="red2-bg">
                                                <th class="line-cent">Product / VAS</th>
                                                <th class="text-center1">Amount <br/>(Mn IDR)</th>
                                                <th class="text-center1">Loyalty <br/>Point</th>
                                                <th class="text-center1 line-cent">Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($products as $product) :
                                                ?>
                                                <tr>
                                                    <td class="text-center td-space">
                                                        <div class="btn-table">
                                                            <div class="label label-success"><?php echo $product->product_name; ?></div>
                                                        </div>
                                                        <div class="btn-table-icon">
                                                            <div class="icon_play"><a data-toggle="modal" data-target="#myModal4">
                                                            <i class="fa fa-play-circle-o"></i></a></div>
                                                            <div class="icon_pdf">
                                                            <a href="<?php echo base_url(); ?>assets/support/salestoolkit.pdf" target="_blank">
                                                            <i class="fa fa-file-pdf-o"></i>
                                                            </a></div>
                                                        </div>

                                                    </td>
                                                    <?php $DisArray = array('4', '5', '6'); ?>
                                                    <td class="col-lg-2">
                                                        <input type="text" name="<?php echo $product->product_id; ?>" id="amount<?php echo $product->product_id; ?>" class="form-control amount-box" onkeyup="this.value = minmax(this.value, 0, 2000)" placeholder="" value="<?php echo $product->amount; ?>" <?php
                                                        if ($product->amount == 'N/A') {
                                                            echo "disabled";
                                                        }
                                                        ?> maxlength="4">
                                                    </td>
                                                    <td class="col-lg-2">
                                                        <input type="text" id="loyal<?php echo $product->product_id; ?>" disabled="" class="form-control point-box" placeholder="" value="<?php echo $product->loyality_point; ?>">
                                                    </td>
                                                    <td class="text-center1">
                                                        <a href="javascript:void(0);" id="<?php echo $product->amount; ?>" class="withdraw remove-product" name="<?php echo $product->loyality_point; ?>">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </td>
                                                </tr> 
                                                <?php
                                            endforeach;
                                            ?>
                                            <?php
                                            if (!empty($additional_product)) {
                                                ?>
                                                <tr>
                                                    <td colspan="4"><h4 class="pp">Additional Products</h4></td>
                                                </tr>   
                                                <?php
                                            }
                                            foreach ($additional_product as $productAdditional) :
                                                ?>
                                                <tr>
                                                    <td class="text-center">
                                                        <div class="btn-table">
                                                            <div class="label label-success"><?php echo $productAdditional->product_name; ?></div>
                                                        </div>
                                                        <div class="btn-table-icon">
                                                            <div class="icon_play"><a data-toggle="modal" data-target="#myModal4"><i class="fa fa-play-circle-o"></i></a></div>
                                                            <div class="icon_pdf"><a href="<?php echo base_url(); ?>/assets/support/salestoolkit.pdf" target="_blank"><i class="fa fa-file-pdf-o"></i></a></div>
                                                        </div>

                                                    </td>
                                                    <?php //$DisArray = array('4', '5', '6');  ?>
                                                    <td class="col-lg-2">
                                                        <input type="text" name="<?php echo $productAdditional->product_id; ?>" id="amount<?php echo $productAdditional->product_id; ?>" class="form-control amount-box" placeholder="" value="<?php echo $productAdditional->amount; ?>" <?php
                                                        if ($productAdditional->amount == 'N/A') {
                                                            echo "disabled";
                                                        }
                                                        ?> maxlength="4" onkeyup="this.value = minmax(this.value, 0, 2000)">
                                                    </td>
                                                    <td class="col-lg-2">
                                                        <input type="text" id="loyal<?php echo $productAdditional->product_id; ?>" disabled="" class="form-control point-box" placeholder="" value="<?php echo $productAdditional->loyality_point; ?>">
                                                    </td>
                                                    <td class="text-center1">
                                                        <a href="javascript:void(0);" id="<?php echo $productAdditional->amount; ?>" class="withdraw remove-product" name="<?php echo $productAdditional->loyality_point; ?>">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </td>
                                                </tr> 
                                                <?php
                                            endforeach;
                                            ?>    
                                        </tbody>
                                        <tfoot class="lightgreen-bg">
                                            <tr>
                                                <td colspan="4">
                                                    
                                                    <div class="total-amounts">
<!--                                                         <h3 class="text-center">Total Amount</h3> -->
                                                        <span id="total_amount_col"><?php //echo $totals->total_amount;    ?></span>
                                                    </div>
                                                    <div class="total-amounts">
                                                        <h3 class="text-center">Total Loyalty Point</h3>
                                                        <span id="total_points_col">25000<?php //echo $totals->total_points;    ?></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-sm-5 col-md-5">
                    <div class="ibox float-e-margins22">
                        <div class="ibox-title">
                            <h5 class="heading-ipad">Documents Required for Application</h5>
<!--                             <a class="pull-right edit-icon edit-form-app" id="EditForm"><i class="fa fa-edit"></i></a> -->
                        </div>
                        <div class="ibox-content too-list">
                            <div id="content-md1" class="content">
                                <ul class="todo-list">
                                    <?php foreach ($documents as $docs) { ?>
                                        <li>
                                            <span><?php echo $docs->document_name; ?></span>
                                            <small>
<!--                                                 <a name="23" class="withdraw1" id="23" href="javascript:void(0);"> -->
<!--                                                     <i class="fa fa-times"></i> -->
                                                </a>
                                            </small>
                                            <?php /* <a class="pull-right edit-icon edit-doc" data-toggle="modal" data-target="#edit" name="<?php echo $docs->document_name; ?>" id="<?php echo $docs->document_id; ?>"><i class="fa fa-edit"></i></a> */ ?>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>        
            </div>
        </div>
        <div class="lead-btns">
            <a href="<?php echo base_url('sales/product'); ?>" class="btn btn-w-m btn-info "><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a> 
            <button class="btn btn-w-m btn-warning" type="button"  data-toggle="modal" data-target="#myEmailModal">Email Details</button> &nbsp;    
            <a href="<?php echo base_url('sales/bundle'); ?>" class="btn btn-w-m btn-danger ">Restart your Selection</a> &nbsp;     
            <button class="btn btn-w-m btn-info finish-product" type="button">Save your Selection</button>
        </div>
    </div>
</div>

<div class="modal inmodal" id="myEmailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Email Detail</h4>
            </div>
            <div class="modal-body">
                <div class="form-email">
                    <form method="post">
                        <span class="error-email"></span>
                        <input type="email" name="email" class="form-control" id="email-pro" placeholder="Enter your Email">
                        <div class="text-align-center">
                            <button class="btn btn-w-m btn-info finish-email-details" type="button">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="form-message" style="display:none;">
                    <p>Product Selection Summary has been Sent Successfully.</p>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal inmodal" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <div class="form-email">
                    <form method="post">
                        <input type="text" name="document_name" class="form-control" id="document_name" placeholder="Enter document name" value = "">
                        <input type="hidden" name="document_id" class="form-control" id="document_id">
                        <div class="text-align-center">
                            <button class="btn btn-w-m btn-info finish-email-details" type="submit">Save</button>
                        </div>
                    </form>
                </div>
                <div class="form-message" style="display:none;">
                    <p>Product Selection Summary has been Sent Successfully.</p>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal inmodal" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <div class="form-email">
                    <form method="post">

                        <input type="email" name="email" class="form-control" id="email-pro" placeholder="Enter document name" value  = "">
                        <div class="text-align-center">
                            <button class="btn btn-w-m btn-info finish-email-details" type="submit">Save</button>
                        </div>
                    </form>
                </div>
                <div class="form-message" style="display:none;">
                    <p>Product Selection Summary has been Sent Successfully.</p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Dmobile</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/9ZguE_UWHj4" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>


    </div>
</div>