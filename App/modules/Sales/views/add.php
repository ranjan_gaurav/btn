<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add new lead</h5>
            </div>
            <div class="ibox-content">
                <form id="form" class="wizard-big" method="post">
                    <h1>Leads</h1>
                    <fieldset>
                        <h2>Lead Information</h2>
                        <div class="row">
                            <?php if (@$error): ?>
                                <div class="alert">
                                    <button type="button" class="close" data-dismiss="alert">�</button>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Category Name *</label>
                                    <input id="category_name" name="category_name" type="text"
                                           class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Contact *</label>
                                    <input id="contact" name="contact" type="text" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Area *</label>
                                    <input id="area" name="area" type="text" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Product Interest *</label>
                                    <input id="product_interest" name="product_interest" type="text"
                                           class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Lead Head *</label>
                                    <input id="lead_head" name="lead_head" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Head Area *</label>
                                    <input id="head_area" name="head_area" type="text" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Capacity *</label>
                                    <input id="capacity" name="capacity" type="text" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Product Ranking *</label>
                                    <input id="product_ranking" name="product_ranking" type="text"
                                           class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label>Sector Ranking *</label>
                                    <input id="sector_ranking" name="sector_ranking" type="text"
                                           class="form-control required">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

</div>