<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Daily Hunter Dashboard</h2></div>
</div>
<div class="wrapper wrapper-content hunter-dashboard">
    <!--dash bourd starts here-->
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title hunter-title">
                    <h3><strong>Target Canvassing Area</strong></h3>
                </div>
                <div class="ibox-content text-center">
                    <div class="min-height-content">
                        <p class="address-new">
                            3<sup>rd</sup> Floor, Senen Jakarta<br/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title hunter-title">
                    <h3><strong>Route Map</strong></h3>
                </div>
                <div class="ibox-content">
                    <div class="min-height-content">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d89747.73730537544!2d106.84894711563618!3d-6.220526580545566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x100c5e82dd4b820!2sSpecial+Capital+Region+of+Jakarta%2C+Indonesia!5e0!3m2!1sen!2sin!4v1469249653572" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title hunter-title">
                    <h3><strong>Alerts</strong></h3>
                </div>
                <div class="ibox-content content-section">
                    <div id="content-md" class="content">
                        <table class="footable table table-stripped toggle-arrow-tiny no-margins" data-page-size="8">
                            <thead>
                                <tr class="red2-bg"> 
                                    <th>Lead Name</th>
                                    <th>Notification</th>
                                    <th>Escalated to</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($alerts as $alert) : ?>
                                    <tr class="gradeX">
                                        <td><a href="<?php echo base_url('leads/profile/'.$alert->lead_id);?>"><?php echo $alert->title; ?></a></td>
                                        <td><?php echo $alert->description; ?></td>
                                        <td><?php echo $alert->escalated_to; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>                                
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-sm-6 col-md-6">

            <div class="ibox-title hunter-title"><h3><strong>Scheduled Visits</strong></h3> </div>
            <div class="ibox-content content-section1">
                <div class="short-btn text-right">
                    <!--<button class="btn btn-info btn-me" type="button"> Filter</button>
                    <button class="btn btn-info btn-me" type="button"> Sort</button>-->
                </div>
                <div id="" class="content demo-yx">

                    <div class="table-responsive48">
                        <table class="footable table table-stripped toggle-arrow-tiny dataTables-example" data-page-size="8">
                            <thead>
                                <tr class="red2-bg">

                                    <th data-toggle="true">Lead Name</th>
                                    <th>Address</th>
                                    <th class="hide-j">Contact Number</th>
                                    <th>Key Contact</th>
                                    <th>Activity Type</th>
                                    <th>Activity Purpose</th>
                                    <th>Product Interests</th>
                                    <th>Status</th>
                                    <th>Last Appointment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($visits as $visit) : ?>
                                    <tr>
                                        <td><a href="<?php echo base_url('leads/profile/'.$visit->lead_id);?>"><?php echo $visit->name; ?></a></td>
                                        <td><?php echo $visit->area; ?></td>
                                        <td><?php echo $visit->contact; ?></td>
                                        <td><?php echo $visit->key_contact; ?></td>
                                        <td><?php echo $visit->activity_type; ?></td>
                                        <td><?php echo $visit->activity_purpose; ?></td>
                                        <td><?php echo $visit->product_interest; ?></td>
                                        <td><?php echo $visit->status; ?></td>
                                        <td><?php
                                            echo date("d-m-Y", strtotime($visit->last_contracted));
                                            ;
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>        
                        </table>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!--<div class="lead-details-btns text-right">
                <button class="btn btn-w-m btn-info" type="button">Back</button>
            </div> -->
        </div>
    </div> 

</div>
