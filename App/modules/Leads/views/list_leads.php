<table class="table table-striped table-bordered table-hover dataTables-example" >
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Area Code</th>
            <th>Product Interest</th>
            <th>Industry</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($list as $result) : ?>
            <tr class="gradeX">
                <td><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $result->lead_id; ?>" ></td>
                <td><?php echo $result->name; ?></td>
                <td><?php echo $result->area; ?></td>
                <td><?php echo $result->product_interest; ?></td>
                <td><?php echo $result->industry_id; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>