<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Lead Directory</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!--<div class="top-btn-sec"><button class="btn btn-w-m btn-info" type="button">Filter</button></div>-->
                <form method="post" action="<?php echo base_url('leads/edit'); ?>">
                    <div class="ibox-content leads-content">
                        <div class="content demo-yx">
                            <div class="table-responsive5">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <?php
                                            $roleID = @$this->session->userdata('role');
                                            if ($roleID == '3') {
                                                ?>
                                                <th>Hunter Name</th>
                                            <?php } ?>
                                            <th>Lead Name</th>
                                            <th>Industry</th>
                                            <th>Address</th>
                                            <th>Phone Number</th>
                                            <th>Key Contact</th>
                                            <th>Product Interests</th>
                                            <th>Status</th>
                                            <th>Last Appointment</th>
                                            <th>Next Appointment</th>
                                            <th>Withdraw</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $list) : ?>
                                            <tr class="gradeX <?php echo ($list->withdraw == '1') ? 'already-withdrawn' : 'notwithdraw'; ?>">
                                                <td>
                                                    <?php /* <div class="i-checks"><label> <input type="radio" value="option1" name="allcheckleads"> <i></i></label></div> */ ?>
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio<?php echo $list->lead_id; ?>" value="<?php echo $list->lead_id; ?>" required>
                                                        <label for="radio<?php echo $list->lead_id; ?>">
                                                        </label>
                                                    </div>
                                                </td>
                                                <?php
                                                $roleID = @$this->session->userdata('role');
                                                if ($roleID == '3') {
                                                    ?>
                                                    <td><?php echo ($list->hunter_name!='')?$list->hunter_name:'Komarudin'; ?></td>
                                                <?php } ?>
                                                <td><a href="<?php echo base_url('leads/profile/' . $list->lead_id); ?>"><?php echo $list->name; ?></a></td>
                                                <td><?php echo $list->industry_id; ?></td>
                                                <td><?php echo $list->area; ?></td>
                                                <td class="center"><?php echo $list->contact; ?></td>
                                                <td><?php echo $list->key_person; ?></td>
                                                <td class="center"><?php echo $list->product_interest; ?></td>
                                                <td class="center"><?php echo $list->product_ranking; ?></td>
                                                <td class="center"><?php echo $list->last_appointment; ?></td>
                                                <td class="center"><?php echo $list->next_appointment; ?></td>
                                                <td class="center">
                                                    <a href="javascript:void(0);" id="<?php echo $list->lead_id; ?>" class="<?php echo ($list->withdraw == '1') ? 'withdrawn' : 'withdraw'; ?>" name="<?php echo $list->lead_id; ?>">
                                                        <i class="fa <?php echo ($list->withdraw == '1') ? 'fa-check' : 'fa-times'; ?>"></i>
                                                    </a>
                                                    <?php /* <input id="<?php echo $list->lead_id; ?>" class="<?php echo ($list->withdraw == '1') ? 'withdrawn' : 'withdraw'; ?>" type="checkbox" <?php echo ($list->withdraw == '1') ? 'checked' : ''; ?> name="<?php echo $list->lead_id; ?>"> */ ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <input type="hidden" id="P_withdrawurl" value="<?php echo base_url('leads/withdraw'); ?>" />
                                <input type="hidden" id="P_responseDiv" value="responce_container" />
                            </div>
                        </div>
                    </div>
                    <div class="lead-btns">
                        <a href="<?php echo base_url('leads/addlead'); ?>" class="btn btn-w-m btn-info">New Lead</a>
                        <button class="btn btn-w-m btn-warning refer-lead" type="submit">Edit Lead</button>
                        <a href="<?php echo base_url('activity'); ?>" class="btn btn-w-m btn-primary">Plan Activity</a>
                        <?php /* <button class="btn btn-w-m btn-info" type="button">Go to Back</button> */ ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>