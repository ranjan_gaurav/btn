<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Edit Lead Form</h2></div>
</div>
<?php if (@$error && $this->input->post('error_show') == '1'): ?>
    <div class="panel panel-warning">
        <div class="panel-heading">
            <i class="fa fa-warning"></i> Warning Panel
        </div>
        <div class="panel-body">
            <p><?php //echo $error;              ?>Please fill all mandatory fields.</p>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('message')) { ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Success Panel
        </div>
        <div class="panel-body">
            <p><?php echo $this->session->flashdata('message'); ?></p>
        </div>
    </div>
<?php } ?>

<form id="leadform" class="wizard-big" method="post">
    <div class="row">
        <div class="lead-forms-area">
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Sales Staff ID</label>
                    <input type="text" class="form-control" placeholder="EM-005" disabled name="sales_staff_id" value="2873"> 
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead Creation Date:</label> 
                    <input type="text" class="form-control" placeholder="04-05-2016" disabled name="creation_date" value="<?php echo date('Y-m-d'); ?>"> 
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label class="red-high">Lead Source*:</label> 
                    <select class="selectpicker form-control required" name="lead_source_id">
                        <option value=''>Select</option>
                        <?php /* foreach ($company as $clist): ?>
                          <option value="<?php echo $clist->company_id; ?>"><?php echo $clist->name; ?></option>
                          <?php endforeach; */ ?>
                        <option value='self-created' <?php
                        if ($lead->lead_source_id == 'self-created') {
                            echo "selected";
                        }
                        ?>>Self-Created</option>
                        <option value='mgm' <?php
                        if ($lead->lead_source_id == 'mgm') {
                            echo "selected";
                        }
                        ?>>MGM</option>
                        <option value='internal-referral' <?php
                        if ($lead->lead_source_id == 'internal-referral') {
                            echo "selected";
                        }
                        ?>>Internal Referrals</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead ID:</label>
                     <input type="text" name="lead_id" class="form-control" value= "<?php echo $lead->lead_id; ?>" readonly="readonly" > 
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Segment:</label> 
                    <select class="selectpicker form-control required" name="segment">
                        <option value=''>Select Segment</option>
                        <option value="SME" <?php
                        if ($lead->segment == 'SME') {
                            echo "selected";
                        }
                        ?>>SME</option>
                        <option value="Commercial" <?php
                        if ($lead->segment == 'Commercial') {
                            echo "selected";
                        }
                        ?>>Commercial</option>
                        <option value="Individual" <?php
                        if ($lead->segment == 'Individual') {
                            echo "selected";
                        }
                        ?>>Individual</option>
                        <option value="Other" <?php
                        if ($lead->segment == 'Other') {
                            echo "selected";
                        }
                        ?>>Other</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead Status:</label> 
                    <select class="selectpicker form-control required" name="segment_id">
                        <option value="0" <?php
                        if ($lead->segment_id == '0') {
                            echo "selected";
                        }
                        ?>>Select Status</option>
                        <option value="1" <?php
                        if ($lead->segment_id == '1') {
                            echo "selected";
                        }
                        ?>>Assigned</option>
                        <option value="2" <?php
                        if ($lead->segment_id == '2') {
                            echo "selected";
                        }
                        ?>>Interested</option>
                        <option value="3" <?php
                        if ($lead->segment_id == '3') {
                            echo "selected";
                        }
                        ?>>Closed</option>
                    </select>
                </div>
            </div>	
        </div>
        <div class="lead-forms-section">
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Company Profile</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group lead-forms">
                            <label class="red-high">Name*</label>
                            <input type="text" placeholder="Enter Name" class="form-control required" name="name" value="<?php echo $lead->name; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label class="red-high">Industry*</label> 
                            <select class="selectpicker form-control required" name="industry_id">
                                <option value="Industry" <?php
                                if ($lead->industry_id == 'Industry') {
                                    echo "selected";
                                }
                                ?>>Industry</option>
                                <option value="Restaurant" <?php
                                if ($lead->industry_id == 'Restaurant') {
                                    echo "selected";
                                }
                                ?>>Restaurant</option>
                                <option value="F&B" <?php
                                if ($lead->industry_id == 'F&B') {
                                    echo "selected";
                                }
                                ?>>F&B</option>
                                <option value="FMCG" <?php
                                if ($lead->industry_id == 'FMCG') {
                                    echo "selected";
                                }
                                ?>>FMCG</option>
                                <option value="Autoparts" <?php
                                if ($lead->industry_id == 'Autoparts') {
                                    echo "selected";
                                }
                                ?>>Autoparts</option>
                                <option value="Retails" <?php
                                if ($lead->industry_id == 'Retails') {
                                    echo "selected";
                                }
                                ?>>Retails</option>
                                <option value="Tourism" <?php
                                if ($lead->industry_id == 'Tourism') {
                                    echo "selected";
                                }
                                ?>>Tourism</option>
                                <option value="Others" <?php
                                if ($lead->industry_id == 'Others') {
                                    echo "selected";
                                }
                                ?>>Others</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms">
                            <label>Sales (Mn IDR)</label>
                            <input type="text" placeholder="" class="form-control required" name="sales" value="<?php echo $lead->sales; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label>Assets (Mn IDR)</label>
                            <input type="text" placeholder="" class="form-control required" name="assets" value="<?php echo $lead->assets; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label>Indebtedness</label>
                            <input type="text" placeholder="" class="form-control" name="indebtedness" value="<?php echo $lead->indebtedness; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label>Key Person</label>
                            <input type="text" placeholder="" class="form-control" name="key_person" value="<?php echo $lead->key_person; ?>"> 
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Contact Information</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group lead-forms">
                            <label class="red-high">Address*</label>
                            <input type="text" placeholder="Enter Name" class="form-control required" name="address" value="Kav., Jl. M.H. Thamrin No.15, Gondangdia, Jakarta"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label>Branch</label>
                            <input type="text" placeholder="" class="form-control" name="branch" value="<?php echo $lead->branch; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label>Area</label> 
                            <select class="selectpicker" name="area">
                                <option value="Area 1" <?php
                                if ($lead->area == 'Area 1') {
                                    echo "selected";
                                }
                                ?>>Area 1</option>
                                <option value="Area 2" <?php
                                if ($lead->area == 'Area 2') {
                                    echo "selected";
                                }
                                ?>>Area 2</option>
                                <option value="Area 3" <?php
                                if ($lead->area == 'Area 3') {
                                    echo "selected";
                                }
                                ?>>Area 3</option>
                                <option value="Area 4" <?php
                                if ($lead->area == 'Area 4') {
                                    echo "selected";
                                }
                                ?>>Area 4</option>
                                <option value="Area 5" <?php
                                if ($lead->area == 'Area 5') {
                                    echo "selected";
                                }
                                ?>>Area 5</option>
                                <option value="Area 6" <?php
                                if ($lead->area == 'Area 6') {
                                    echo "selected";
                                }
                                ?>>Area 6</option>
                                <option value="Area 7" <?php
                                if ($lead->area == 'Area 7') {
                                    echo "selected";
                                }
                                ?>>Area 7</option>
                                <option value="Area 8" <?php
                                if ($lead->area == 'Area 8') {
                                    echo "selected";
                                }
                                ?>>Area 8</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms">
                            <label>Region</label> 
                            <select class="selectpicker" name="region">
                                <option value="Region 1" <?php
                                if ($lead->region == 'Region 1') {
                                    echo "selected";
                                }
                                ?>>Region 1</option>
                                <option value="Region 2" <?php
                                if ($lead->region == 'Region 2') {
                                    echo "selected";
                                }
                                ?>>Region 2</option>
                                <option value="Region 3" <?php
                                if ($lead->region == 'Region 3') {
                                    echo "selected";
                                }
                                ?>>Region 3</option>
                                <option value="Region 4" <?php
                                if ($lead->region == 'Region 4') {
                                    echo "selected";
                                }
                                ?>>Region 4</option>
                            </select>
                        </div>

                        <div class="form-group lead-forms">
                            <label class="red-high">Phone*</label>
                            <input type="phone" placeholder="" class="form-control required" name="contact" value="<?php echo $lead->contact; ?>"> 
                        </div>
                        <div class="form-group lead-forms">
                            <label class="red-high">Email*</label>
                            <input type="email" placeholder="Enter your email" class="form-control required" name="email" value="client@gmail.com"> 
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Product Interests</h5>
                        <div class="pull-right">
                            <div class="plus-i addMoreProduct">
                                <i class="fa fa-plus-square-o"></i>
                            </div>
                            <div class="pdf-i">
                                <a data-toggle="modal" data-target="#myModal4"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="content" id="slimtest1">
                            <div class="topProductInterest">
                                <div class="productInterest">
                                    <!--<div class="form-group lead-forms">
                                        <label class="">Product Category</label>
                                        <select id = "ddl" class="selectpicker required" name='product1_type' onchange = "configureDropDownLists(this, document.getElementById('ddl2'))">
                                            <option value=''>Select Product</option>
                                            <option value="FundingBundle">Funding Bundle</option>
                                            <option value="FundingOnly">Funding Only</option>
                                            <option value="LendingBundle">Lending Bundle</option>
                                            <option value="LendingOnly">Lending Only</option>
                                            <option value="ValueAddedService">Value Added Service</option>

                                        </select>
                                    </div>-->
                                    <div class="form-group lead-forms">
                                        <label class="">Product Category</label> 
                                        <select class="selectpicker required" name='product_type[]'>
                                            <option value=''>Select Product</option>
                                            <option value="Funding" <?php
                                            if ($lead->product1_type == 'Funding') {
                                                echo "selected";
                                            }
                                            ?>>Funding</option>
                                            <option value="Lending" <?php
                                            if ($lead->product1_type == 'Lending') {
                                                echo "selected";
                                            }
                                            ?>>Lending</option>

                                        </select>
                                    </div>
                                    
                                    <div class="form-group lead-forms">
                                        <label class="">Product Interest</label> 
                                        <select class="selectpicker required" name='product_name[]'>
                                            <option value=''>Product Interest</option>
                                            <option value="term_loan" <?php
                                            if ($lead->product1_name == 'term_loan') {
                                                echo "selected";
                                            }
                                            ?>>Term Loan</option>
                                            <option value="term_loan_bundle" <?php
                                            if ($lead->product1_name == 'term_loan_bundle') {
                                                echo "selected";
                                            }
                                            ?>>Term Loan Bundle</option>
                                            <option value="od_facility" <?php
                                            if ($lead->product1_name == 'od_facility') {
                                                echo "selected";
                                            }
                                            ?>>OD Facility</option>
                                            <option value="od_facility_bundle" <?php
                                            if ($lead->product1_name == 'od_facility_bundle') {
                                                echo "selected";
                                            }
                                            ?>>OD Facility Bundle</option>
                                            <option value="casa" <?php
                                            if ($lead->product1_name == 'casa') {
                                                echo "selected";
                                            }
                                            ?>>CASA</option>
                                            <option value="casa_bundle" <?php
                                            if ($lead->product1_name == 'casa_bundle') {
                                                echo "selected";
                                            }
                                            ?>>CASA Bundle</option>
                                            <option value="td" <?php
                                            if ($lead->product1_name == 'td') {
                                                echo "selected";
                                            }
                                            ?>>TD</option>
                                        </select>
                                        <!--<a href="<?php //echo base_url('assets/support/sales.pdf');       ?>" target="_blank">Details</a>-->
                                    </div>
                                    <div class="form-group lead-forms">
                                        <label class="line-h">Nominal Amount<br/> (Mn IDR)</label>
                                        <input type="text" placeholder="" class="form-control" name='product_amount[]' value="<?php echo $lead->product1_amount; ?>"> 
                                    </div>

                                    <div class="form-group lead-forms">
                                        <label class="">Status</label>
                                        <select  class="selectpicker required" name='status[]'>
                                            <option value=''>Select Status</option>
                                            <option value="Interested">Interested</option>
                                            <option value="Application prep">Application prep</option>
                                            <option value="Accepted by CF">Accepted by CF</option>
                                            <option value="OD Facility Bundle">OD Facility Bundle</option>
                                            <option value="Approved">Approved</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Appealed">Appealed</option>
                                            <option value="Withdrawn Pre-Application">Withdrawn Pre-Application</option>
                                            <option value="Withdrawn Post-Application">Withdrawn Post-Application</option>
                                            <option value="Referred">Referred</option>
                                            <option value="Disbursed">Disbursed</option>
                                        </select>
                                    </div>
                                    <?php /* <a href="#" class="btn btn-w-m btn-primary">Go to Report</a>
                                      <a href="#" class="btn btn-w-m btn-warning"  data-toggle="modal" data-target="#myModal4">Historical Report</a> */ ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lead-details">
            <span>Notes:</span>
            <textarea name="notes" width="100%" class="form-control" cols="20" rows="2"><?php echo $lead->notes; ?></textarea>

        </div>
        <div class="lead-btns">
            <input type="hidden" name="" value="1">

            <input type="hidden" name="lead" value='<?php echo $lead->lead_id; ?>'>
            <button type="submit" name = "referleads" value = "saveLeads" class="btn btn-w-m btn-primary">Save</button>
            <button type="submit" name = "referleads" value = "referleads" class="btn btn-w-m btn-warning refer-lead">Refer Lead</button>
            <a href="<?php echo base_url('leads/index/'); ?>" class="btn btn-w-m btn-info"> Cancel</a>

        </div>
    </div>
    <form>


        <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Report</h4>
                    </div>
                    <div class="modal-body">
                        <p class="infor"><strong>Visit 25.5.16</strong> <br/>Discussed overdraft. Prospect agreed to apply.<br/></p>
						<div class="hr-line-dashed"></div>
                       <p class="infor"><strong>Visit 25.5.16</strong> <br/>Provided Prospect with information on term loan and overdraft. Prospect asked for time to consider<br/></p>
					   <div class="hr-line-dashed"></div>
                       <p class="infor"><strong>Call 25.5.16</strong><br/>Prospect interested in lending products. Agreed to follow-up with more information on Danamon options.<br/></p>
                    </div>
                </div>

            </div>
        </div>
