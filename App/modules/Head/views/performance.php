<?php error_reporting(0);?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Hunter Performance Dashboard Head Office</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row padding-bottom-area">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content refaral-e refaral-0e">
			<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
                <div class="form-group refaral-form">
                    <label>Level view:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">National</option>
                        <option value="2">Regional</option>
                        <option value="3">Area</option>
                    </select>
                </div>
            </div>
			<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
				<div class="form-group refaral-form">
                    <label>Unit / Staff:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Team-01</option>
                        <option value="2">Team-02</option>
                        <option value="3">Team-03</option>
                        <option value="4">Team-04</option>
                        <option value="5">Team-05</option>
						<option value="5">Team-06</option>
                    </select>
                </div>
				</div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">	
                <div class="form-group refaral-form">
                    <label>PIC</label>
                    <small>Sumit</small>
                </div>
				</div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
                <div class="form-group refaral-form">
                    <label>Region</label>
                    <small>R1</small>
                </div>
                </div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
				<div class="form-group refaral-form">
                    <label>Area</label>
                    <small>Area A</small>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content refaral-e">
			<div class="col-lg-12 col-sm-12 col-md-12 padding-r1">
			<div class="pull-right form-pit">
                <div class="form-group refaral-form">
                    <label>Rank</label>
                    <select name="Company_Name" class="selectpicker"  onChange="showUser(this.value);">
                      
                        <option value="0">Region</option>
                        <option value="2">Area</option>
                        <option value="3">Team</option>
                        <option value="4">Hunter</option>
                    </select>
                </div>
                </div>
				</div>
				<div class="clearfix"></div>
                
				<div class="content-scrol12">
                    <div class="full-height-scroll">
                        <div class="table-responsive"  id="txtHint">
                          
                         <?php 
                        // $dataVal['val']='0';
                         $this->load->view('load-table');
                         ?>
                                          
                                        
                                      
                                  
                                       
                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>KPIs</h5>
                </div>
                <div class="ibox-content heightcss">
                    <div class="financial_kpi">
					<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunterleader/hunter_performance_dash.svg" height="260" class="img-t" alt="financial_kpi">
					<!--<div class="line"></div>
                         <div id="chartContainer"></div>
         <canvas id="ntbChart" height="160"></canvas> 
        <div class="legends">
            <div class="target"><i class="fa fa-square"></i> Target</div>
            <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-4 chart-padding-middle">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Incentive Scheme</h5>
                </div>
                <div class="ibox-content heightcss">
				<div class="process-indicators15">
                        <div id="content-md1" class="content">
                    <div class="col-lg-6 col-sm-6 chart-padding-right">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Sold Loan Bundle to NTB</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="loanbunddlechart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunterleader/Sold_Loan_Bundle_to_NTB.svg" class="img-t" alt="financial_kpi" height="100">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                     <div id="chartContainer1"></div>
       <canvas id="loanbundleChart" height="140"></canvas> 
    
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 chart-padding-left">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Sold Loan Only to NTB</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="ntbwithLoanChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunterleader/Sold_Loan_Only_to_NTB.svg" class="img-t" alt="financial_kpi" height="100">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                    <div id="chartContainer2"></div>
          <canvas id="ntbwithLoanChart" height="140"></canvas> 
    </div>
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
<div class="col-lg-6 col-sm-6 chart-padding-right">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Sold CASA</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="refferalChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunterleader/Sold_CASA.svg" class="img-t" alt="financial_kpi" height="100">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div> 
								<div id="chartContainer4"></div>
                                 <div id="chartContainer4">
  </div>
                                                                    <!-- <canvas id="refferalChart" height="140"></canvas> 
                                 </div>
                                 <div class="legends">
                                     <div class="target"><i class="fa fa-square"></i> Target</div>
                                     <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 chart-padding-left">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Successful Referrals</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="refferalChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunterleader/Successful_Referrals.svg" class="img-t" alt="financial_kpi" height="100">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div> 
								<div id="chartContainer4"></div>
                                 <div id="chartContainer4">
  </div>
                                                                    <!-- <canvas id="refferalChart" height="140"></canvas> 
                                 </div>
                                 <div class="legends">
                                     <div class="target"><i class="fa fa-square"></i> Target</div>
                                     <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 padding-none-all">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Sold Loan to Non-Loan ETB Served by Funding RO/RM</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="ntbwithoutLoanChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/served_by_funding_RORM.svg" class="img-t" alt="financial_kpi" height="100">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                    <div id="chartContainer3"></div>
         <canvas id="ntbwithoutLoanChart" height="140"></canvas> 
    </div>
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Process Indicators</h5>
                </div>
                <div class="ibox-content heightcss">
                    <!--<div class="row">
                        <div class="col-lg-6 col-sm-12 col-md-12 padding-off-right sd">
                            <div class="form-group data_decade_view1" id="data_decade_view1">
                                <div class="input-group date">
                                    <span class="input-group-addon">Start Date</span><input type="text" class="form-control" value="03/04/2014">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-sm-12 col-md-12 ed">
                            <div class="form-group data_decade_view1" id="data_decade_view2">
                                <div class="input-group date">
                                    <span class="input-group-addon">End Date</span><input type="text" class="form-control" value="03/04/2014">
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="process-indicators12">
                        <div id="content-md" class="content">
						<div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Indicator</th>
                                        <th>This Week</th>
                                        <th>Month-to-Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lead Created</td>
                                        <td>240</td>
										<td>7</td>
                                        
                                    </tr>
                                    <tr>
                                        <td>Visits</td>
                                        <td>960</td>
                                        <td>30</td>
                                    </tr>
                                    <tr>
                                        <td>Applications Accepted by CF</td>
                                        <td>240</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>
                                        <td>Applications Approved by CF</td>
                                        <td>80</td>
                                        <td>4</td>
                                    </tr>
                                </tbody>
                            </table>
							<table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Indicator</th>
                                        <th>Last Month</th>
										<th>Last Three Months</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lead Conversion Rate</td>
                                        <td>12%</td>
                                        <td>8%</td>
                                    </tr>
                                    <tr>
                                        <td>Avg. Visits Per Lead</td>
                                        <td>3</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td> TAT to Acceptance by CF</td>
                                        <td>8</td>
                                        <td>10</td>

                                    </tr>
                                    <tr>
                                        <td>TAT from Acceptance by CF to Disbursement</td>
                                        <td>4</td>
                                        <td>3</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
						</div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>                
        </div>
    </div>


</div>
<div class="flot-chart" style="display:none;">
    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
</div>


