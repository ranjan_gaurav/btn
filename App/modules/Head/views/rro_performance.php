<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRM Performance Dashboard – Head Office</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row space-btm">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content refaral-form2">
			<div class="refaral-form-h">
			<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
                <div class="form-group refaral-form">
				<label>Level view:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
				</div> 
				<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
                <div class="form-group refaral-form">
                    <label>Unit / Staff:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
            </div> 
			<div class="clearfix">&nbsp;</div>
            </div>
			</div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content">
			<div class="refaral-form-h">
			<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">			
				<div class="form-group refaral-form">
                    <label>PIC</label>
                    <small>Gabelas</small>
                </div>
				</div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
                <div class="form-group refaral-form">
                    <label>Region</label>
                    <small>R1</small>
                </div>
                </div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
				<div class="form-group refaral-form">
                    <label>Area</label>
                    <small>Area A</small>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
				</div>
			<!--<div class="col-lg-5 col-sm-10 col-md-10 padding-r1 pull-right">
                <div class="form-group refaral-form">
                    <label>Rank</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
                <div class="content-scrol2">
                    <div class="full-height-scroll">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Rank</th>
                                        <th>Staff / Unit</th>
                                        <th>Sales (M IDR)</th>
                                        <th>Conversion Rate (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Team1</td>
                                        <td>60,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Team2</td>
                                        <td>76,000</td>
                                        <td>82%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Team3</td>
                                        <td>70,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Team4</td>
                                        <td>86,000</td>
                                        <td>82%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Team5</td>
                                        <td>65,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Team6</td>
                                        <td>70,000</td>
                                        <td>82%</td>
                                    </tr>					
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            -->
			</div>
        </div>
    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
<div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content rro-performance">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="bg-g">&nbsp;</th>
                                        <th colspan="2" class="text-center1">Up-sell / Cross-sell</th>
                                        <th colspan="2" class="text-center1">Loan Maintenance</th>
                                        <th colspan="2" class="text-center1">Relationship Calls</th>
                                        <th colspan="2" class="text-center1">Total</th>
                                    </tr>
									<tr>
                                        <th class="bg-g">&nbsp;</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>Tasks assigned this month</td>
                                        <td class="text-center1">3200</td>
                                        <td class="text-center1">25%</td>
                                        <td class="text-center1">1280</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">1920</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">6400</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks overdue</td>
                                        <td class="text-center1">2560</td>
                                        <td class="text-center1">20%</td>
                                        <td class="text-center1">1280</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">1920</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">5760</td>
                                        <td class="text-center1">23%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Newly Assigned</td>
                                        <td class="text-center1">3200</td>
                                        <td class="text-center1">25%</td>
                                        <td class="text-center1">1280</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">1920</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">6400</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks created by RRO (MTD)</td>
                                        <td class="text-center1">3840</td>
                                        <td class="text-center1">30%</td>
                                        <td class="text-center1">1920</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">1280</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">7040</td>
                                        <td class="text-center1">28%</td>
                                    </tr>
                                    <tr class="gradeD">
                                        <td>Total tasks for current month</td>
                                        <td class="text-center1">12800</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">5760</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">7040</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">25600</td>
                                        <td class="text-center1">100%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Successful</td>
                                        <td class="text-center1">3840</td>
                                        <td class="text-center1">60%</td>
                                        <td class="text-center1">2560</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">2560</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">8960</td>
                                        <td class="text-center1">78%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Closed (Successful + Failed)</td>
                                        <td class="text-center1">2560</td>
                                        <td class="text-center1">40%</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">2560</td>
                                        <td class="text-center1">22%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-lg-12 col-sm-12 col-md-12">
		<div class="row">
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Up-sell / Cross-sell</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_04.svg" class="img-t" alt="Up-sell-Cross-sell-rl" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Loan Maintenance</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_05.svg" class="img-t" alt="Loan Maintenance" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Relationship Calls</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_06.svg" class="img-t" alt="Relationship Calls" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		</div>
		</div>
    </div>
</div>

<div class="flot-chart" style="display:none;">
    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
</div>