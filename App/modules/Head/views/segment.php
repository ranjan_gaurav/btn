<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>SME Segment Overview</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4 col-sm-4 col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="form-group" id="data_5">
                        <div class="col-lg-12">
                                <div class="col-lg-6 col-sm-4 col-md-4">
                                    <div class="form-group" id="data_decade_view1">
                                        <div class="input-group date">
                                            <span class="input-group-addon">Start Date</span><input type="text" class="form-control" value="03/04/2016">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-4 col-md-4">
                                    <div class="form-group" id="data_decade_view1">
                                        <div class="input-group date">
                                            <span class="input-group-addon">End Date</span><input type="text" class="form-control" value="03/04/2016">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <div class="col-lg-12 col-sm-12 col-md-12 padding-none-q">
						<div class="ranking-items">
                         <label>Ranking Criteria:</label>
						 <select name="area" class="selectpicker">
						 <option selected>CASA Balance</option>
                         <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                            </select>
                            
							</div>
                        </div>
                    </div>
					<div class="segment">
					<div id="content-md1" class="content">
					<div class="table-responsive">
                    <table class="footable table table-stripped toggle-arrow-tiny no-margins">
                        <thead>
                            <tr class="red2-bg">
                                <th>Rank</th>
                        <th>Region</th>
                        <th>Total CASA (IDR Bn)</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Region 1</td>
                                <td>1,230</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Region 2</td>
                                <td>886</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Region 1</td>
                                <td>1,230</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Region 3</td>
                                <td>936</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
				
				</div>
				</div>
				<div class="clearfix"></div>
				</div>
            </div>
        </div>
        <div class="col-lg-8 col-sm-8 col-md-8">
            <div class="row">
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title">
                            <h5>Total Loan</h5>
                        </div>
                        <div class="ibox-content text-center p-md">
						<div class="chart-img">
                            <img src="<?php echo base_url(); ?>/assets/img/graph/segment/totalloan.jpg" class="img-t" alt="Total Loan">                    
                        </div>
						</div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title">
                            <h5>Total CASA</h5>
                        </div>
                        <div class="ibox-content text-center p-md">
                           <div class="chart-img">
                            <img src="<?php echo base_url(); ?>/assets/img/graph/segment/totalloan.jpg" class="img-t" alt="Total Loan">                    
                        </div>                    
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title">
                            <h5>New Customer Acquisitions</h5>
                        </div>
                        <div class="ibox-content text-center p-md">
                            <div class="chart-img">
                            <img src="<?php echo base_url(); ?>/assets/img/graph/segment/totalloan.jpg" class="img-t" alt="Total Loan">                    
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center endup">
                    <div class="lagents">
					<span class="small-sme"><img src="<?php echo base_url(); ?>/assets/img/graph/segment/target.png" class="img-t" alt="Total Loan">  Target</span>
                     <span class="small-sme"><i class="fa fa-square fa-1x" style="color:#ffc321"></i>Small SME</span>
                     <span class="medium-sme"><i class="fa fa-square fa-1x" style="color:#f8941c"></i>Medium SME</span>
                     <span class="large-sme"><i class="fa fa-square fa-1x" style="color:#f36a3e"></i>Large SME</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">            
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6  col-sm-12 col-md-6">                
                            <div class="dashed-border-red">
                                <h3>HR Metrics</h3>
                                <table class="table no-margins text-center">
                                    <thead>
                                        <tr class="red2-bg">
                                            <th> </th>
                                            <th>New Hire</th>
                                    <th><p class="text-center attrition">Target</p></th>
                                    <th><p class="text-center">Head Count</p></th>
                                    <th><p class="text-center">Target</p></th>
                                    <th><p class="text-center">Attrition</p></th>
									<th><p class="text-center">Target</p></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="bg-info">
                                            <td> Hunter</td>
                                            <td>45</td>
                                            <td>50</td>
                                            <td>120</td>
                                            <td>133</td>
                                            <td>50%</td>
											<td>50%</td>
                                        </tr>
                                        <tr class="bg-info">
                                            <td>Hunter Leader</td>
                                            <td>5</td>
                                            <td>5</td>
                                            <td>12</td>
                                            <td>11</td>
                                            <td>80%</td>
											<td>85%</td>
                                        </tr>
                                        <tr class="bg-danger">
                                            <td>RRM</td>
                                            <td>30</td>
                                            <td>33</td>
                                            <td>60</td>
                                            <td>65</td>
                                            <td>85%</td>
											<td>80%</td>
                                        </tr>
                                        <tr class="bg-danger">
                                            <td>RRM Leader</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>7</td>
                                            <td>10</td>
                                            <td>100%</td>
											<td>90%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-lg-6  col-sm-6 col-md-6">

                            <div class="dashed-border-red">
                                <h3>Process Metrics </h3>                    
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                     <div class="chart-img">
										<img src="<?php echo base_url(); ?>/assets/img/graph/segment/Process-Metrics.png" class="img-t" alt="Total Loan">                    
										</div>
										<div class="lagents-new">
										<span>Lead to<br/>App. Rate</span>
										<span>Application<br/>to Approval</span>
										<span>Approval<br/>to Onboard</span>
										</div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                    <div class="chart-img">
										<img src="<?php echo base_url(); ?>/assets/img/graph/segment/Process-Metrics1.png" class="img-t" alt="Total Loan">                    
										</div> 
										<div class="lagents-new2">
										<span>TAT (Lead to App)</span>
										<span>TAT (App to Onboard)</span>
										</div>
                                    </div>
                                </div>
                            </div>                
                        </div>                
                    </div>
                </div>                          
            </div>
        </div>
    </div>
</div>
