<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRM Performance Dashboard – Head Office</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row space-btm">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content refaral-form2">
			<div class="refaral-form-h">
			<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
                <div class="form-group refaral-form">
				<label>Level view:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
				</div> 
				<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
                <div class="form-group refaral-form">
                    <label>Unit / Staff:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
            </div> 
			<div class="clearfix">&nbsp;</div>
            </div>
			</div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox-content">
			<div class="refaral-form-h">
			<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">			
				<div class="form-group refaral-form">
                    <label>PIC</label>
                    <small>Gabelas</small>
                </div>
				</div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-r1">
                <div class="form-group refaral-form">
                    <label>Region</label>
                    <small>R1</small>
                </div>
                </div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-12 col-md-12 padding-l1">
				<div class="form-group refaral-form">
                    <label>Area</label>
                    <small>Area A</small>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
				</div>
			<!--<div class="col-lg-5 col-sm-10 col-md-10 padding-r1 pull-right">
                <div class="form-group refaral-form">
                    <label>Rank</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Area-01</option>
                        <option value="2">Area-02</option>
                        <option value="3">Area-03</option>
                        <option value="4">Area-04</option>
                        <option value="5">Area-05</option>
                    </select>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
                <div class="content-scrol2">
                    <div class="full-height-scroll">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Rank</th>
                                        <th>Staff / Unit</th>
                                        <th>Sales (M IDR)</th>
                                        <th>Conversion Rate (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Team1</td>
                                        <td>60,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Team2</td>
                                        <td>76,000</td>
                                        <td>82%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Team3</td>
                                        <td>70,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Team4</td>
                                        <td>86,000</td>
                                        <td>82%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Team5</td>
                                        <td>65,000</td>
                                        <td>80%</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Team6</td>
                                        <td>70,000</td>
                                        <td>82%</td>
                                    </tr>					
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            -->
			</div>
        </div>
    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
<div class="row">
       <div class="col-lg-6 col-sm-6 col-md-6 chart-padding-middle">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Financial KPIs</h5>
                </div>
                <div class="ibox-content">
                    <div class="col-lg-6 col-sm-6 chart-padding-right">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Overdraft Growth</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="loanbunddlechart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Overdraft-growth.png" class="img-t" alt="Overdraft Growth">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                     <div id="chartContainer1"></div>
       <canvas id="loanbundleChart" height="140"></canvas> 
    
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 chart-padding-left">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Term Loan Growth</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="ntbwithLoanChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Term-loan-growth.png" class="img-t" alt="Term Loan Growth">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                    <div id="chartContainer2"></div>
          <canvas id="ntbwithLoanChart" height="140"></canvas> 
    </div>
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 chart-padding-right">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>CASA Balance Growth</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="ntbwithoutLoanChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/rrm/CASA-balance-growth.png" class="img-t" alt="CASA Balance Growth">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div>
                                    <div id="chartContainer3"></div>
         <canvas id="ntbwithoutLoanChart" height="140"></canvas> 
    </div>
    <div class="legends">
        <div class="target"><i class="fa fa-square"></i> Target</div>
        <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 chart-padding-left">
                        <div class="grap-content">
                            <div class="ibox-title">
                                <h5>Other Cross-Sell</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="refferalChart">
								<img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Other-Cross-sell.png" class="img-t" alt="Other Cross-sell">
								<!--<div class="line1"></div>
								<div class="line2"></div>
								<div class="line3"></div> 
								<div id="chartContainer4"></div>
                                 <div id="chartContainer4">
  </div>
                                                                    <!-- <canvas id="refferalChart" height="140"></canvas> 
                                 </div>
                                 <div class="legends">
                                     <div class="target"><i class="fa fa-square"></i> Target</div>
                                     <div class="achievement"><i class="fa fa-square"></i> Achievement</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
   
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Non-Financial KPIs</h5>
                </div>
                <div class="ibox-content">
                    <div class="financial_kpi">
					<h5>Risk Management</h5>
					<div class="risk-management">
					<img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Riskmanagement.png" class="img-t" alt="Risk Management">
					</div>
					<!--<div class="line"></div>
                         <div id="chartContainer"></div>
         <canvas id="ntbChart" height="160"></canvas> 
        <div class="legends">
            <div class="target"><i class="fa fa-square"></i> Target</div>
            <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
        </div> -->
                    </div>
                </div>
            </div>
        </div>

 </div>
</div>

<div class="flot-chart" style="display:none;">
    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
</div>