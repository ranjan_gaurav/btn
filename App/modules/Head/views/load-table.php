
<table class="table table-striped table-bordered table-hover dataTables-example">
<thead>
                                        <tr>
                                            <th>Staff / Unit</th>
                                            <th>Loan Sales (Bn IDR)</th>
                                            <th>Conversion Rate (%)</th>
                                            
                                          
                                           
                                        </tr>
                                    </thead>
                                  <tbody>

                               
                                <?php if($val == 0) {?>
                                       <tr >
                                        
                                        <td>Region 1</td>
                                        <td>15000</td>
                                        <td>12%</td>
                                    </tr>
                                    <tr >
                                        
                                        <td>Region 2</td>
                                        <td>12750</td>
                                        <td>11%</td>
                                    </tr>
                                    <tr >
                                        
                                        <td>Region 3</td>
                                        <td>11,390</td>
                                        <td>10%</td>
                                    </tr>
                                    <?php }?>
                                    <?php if($val == 2) {?>
                                    <tr >
                                        
                                        <td>S&D-001</td>
                                        <td>3200</td>
                                        <td>12%</td>
                                    </tr>
                                    <tr >
                                        
                                        <td>S&D-002</td>
                                        <td>2700</td>
                                        <td>13%</td>
                                    </tr>
                                    <tr>
                                        
                                        <td>S&D-003</td>
                                        <td>2500</td>
                                        <td>15%</td>
                                    </tr>
                                    <?php }?>
                                         <?php if($val == 3) {?>
                                    <tr >
                                        
                                        <td>Team 1</td>
                                        <td>600</td>
                                        <td>12%</td>
                                    </tr>
                                    <tr >
                                        
                                        <td>Team 2</td>
                                        <td>550</td>
                                        <td>13%</td>
                                    </tr>
                                    <tr>
                                        
                                        <td>Team 3</td>
                                        <td>535</td>
                                        <td>11%</td>
                                    </tr>
                                    <?php }?>			
                                    <?php if($val == 4) {?>
                                    <tr>
                                        
                                        <td>RRO 1</td>
                                        <td>60</td>
                                        <td>15%</td>
                                    </tr>
                                    <tr>
                                        
                                        <td>RRO 2</td>
                                        <td>54</td>
                                        <td>18%</td>
                                    </tr>
                                    <tr>
                                        
                                        <td>RRO 3</td>
                                        <td>50</td>
                                        <td>22%</td>
                                    </tr>
                                    <?php }?>					
                                </tbody>     
    </table>
<?php if ($js_to_load != '') : ?>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/<?php echo $js_to_load;?>">

<?php endif;?>