<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Head Office Performance Dashboard</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <!--- date picker--->
            <div class="col-lg-3 col-sm-5 col-md-5">
                <div class="form-group" id="data_4">
                    <label class="font-noraml">Start Date</label>
                    <div class="form-group" id="performance_decade_view1">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="06/06/2016" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-5 col-md-5">
                <div class="form-group" id="data_7">
                    <label class="font-noraml">End Date</label>
                    <div class="form-group" id="performance_decade_view2">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="06/06/2016" type="text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-4 col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-content lengthcss">
                    <div class="form-group" id="data_5">
                        <div class="col-lg-12 col-sm-12 col-md-12 padding-none-q">
                            <div class="ranking-items">
                                <label>Ranking Criteria:</label> <select name="area" class="selectpicker required"
                                                                         onChange="showUser(this.value);">
                                    <option value="1" selected>Total Loan</option>
                                    <option value="2">Total CASA</option>
                                    <option value="3">New Customer Acquisition</option>

                                </select>

                            </div>
                        </div>
                    </div>


                    <?php $data['val']=1;$this->load->view('data-table',$data);?>


                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-sm-8 col-md-8">
            <div class="row">
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title">
                            <h5>Loan (Bn IDR)</h5>
                        </div>
                        <div class="ibox-content text-center p-md2">
                            <div class="chart-img">
							<object data="<?php echo base_url(); ?>/assets/img/graph/head/scg-01.svg" type="image/svg+xml"></object>
                             <!--<img src="<?php echo base_url(); ?>/assets/img/graph/head/scg-01.svg" class="img-t" alt="financial_kpi"> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title">
                            <h5>CASA Loan (Bn IDR)</h5>
                        </div>
                        <div class="ibox-content text-center p-md2">
                            <div class="chart-img">
							<object data="<?php echo base_url(); ?>/assets/img/graph/head/scg-02.svg" type="image/svg+xml"></object>
                            <!-- <img src="<?php echo base_url(); ?>/assets/img/graph/head/scg-02.svg" class="img-t" alt="financial_kpi">-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins e-margins1">
                        <div class="ibox-title head-title5">
                            <h5>New Customer Acquisitions</h5>
                        </div>
                        <div class="ibox-content text-center p-md2">
                            <div class="chart-img">
							<object data="<?php echo base_url(); ?>/assets/img/graph/head/scg-03.svg" type="image/svg+xml"></object>
                            <!--<img src="<?php echo base_url(); ?>/assets/img/graph/head/scg-03.svg" class="img-t" alt="financial_kpi">-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center endup">
                    <div class="lagents">
                        <span class="small-sme">
						<img src="<?php echo base_url(); ?>/assets/img/graph/segment/target.png" class="img-t" alt="Total Loan"> Target</span> 
						<span class="medium-sme"><i class="fa fa-square fa-1x" style="color: #8abcdc"></i>SSME</span> 
						<span class="large-sme"><i class="fa fa-square fa-1x" style="color: #4288b5"></i>M/LSME</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6  col-sm-6 col-md-6">
                            <div class="dashed-border-red">
                                <h3>HR Metrics</h3>
                                <div class="content demo-yx">
                                    <table class="table table-bordered no-margins text-center">
                                        <thead>
                                            <tr class="red2-bg">
                                                <th></th>
                                                <th class="text-center">New Hires Actual</th>
                                                <th class="text-center">New Hires Target</th>
                                                <th class="text-center">Head Count Actual</th>
                                                <th class="text-center">Headcount Target</th>
                                                <th class="text-center">Attrition Actual</th>
                                                <th class="text-center">Attrition Target</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="bg-info">
                                                <td>Hunter</td>
                                                <td>45</td>
                                                <td>50</td>
                                                <td>120</td>
                                                <td>133</td>
                                                <td>50%</td>
                                                <td>50%</td>
                                            </tr>
                                            <tr class="bg-info">
                                                <td>Hunter Leader</td>
                                                <td>5</td>
                                                <td>5</td>
                                                <td>12</td>
                                                <td>11</td>
                                                <td>80%</td>
                                                <td>85%</td>
                                            </tr>
                                            <tr class="bg-danger">
                                                <td>RRM</td>
                                                <td>30</td>
                                                <td>33</td>
                                                <td>60</td>
                                                <td>65</td>
                                                <td>85%</td>
                                                <td>80%</td>
                                            </tr>
                                            <tr class="bg-danger">
                                                <td>RRM Leader</td>
                                                <td>2</td>
                                                <td>3</td>
                                                <td>7</td>
                                                <td>10</td>
                                                <td>100%</td>
                                                <td>90%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6  col-sm-6 col-md-6">
                            <div class="dashed-border-red">
                                <h3>Process Metrics</h3>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-7 col-md-7">
                                        <div class="chart-img">
                                            <img
                                                src="<?php echo base_url(); ?>/assets/img/graph/segment/Process-Metrics.png"
                                                class="img-t1" alt="Total Loan">
                                        </div>
                                        <div class="lagents-new">
                                            <span>Conversion rate from Interested Submitted to CF</span> 
                                            <span>Application approval rate</span>
                                             <span>Conversion rate from Approved to Disbursed</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-md-5">
                                        <div class="chart-img">
                                            <img
                                                src="<?php echo base_url(); ?>/assets/img/graph/segment/Process-Metrics1.png"
                                                class="img-t1" alt="Total Loan">
                                        </div>
                                        <div class="lagents-new2">
                                            <span>TAT from Interested Submitted to CF</span> 
											<span>TAT from Submitted to CF Disbursed</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
