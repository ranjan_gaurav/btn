<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Performance Dashboard</h2></div>
</div>
<?php
$hunterInfo = getUserInfo(@$this->session->userdata('userid'));
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row padding-bottom-area">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox-content refaral-e refaral-g">
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l1">
                    <div class="form-group refaral-form">
                        <label>Level view:</label>
                        <select name="Company_Name" class="selectpicker">
                            <option value="1">National</option>
                            <option value="2">Regional</option>
                            <option value="3">Area</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-r1">
                    <div class="form-group refaral-form">
                        <label>Unit / Staff:</label>
                        <select name="Company_Name" class="selectpicker">
                            <option value="1">Team-01</option>
                            <option value="2">Team-02</option>
                            <option value="3">Team-03</option>
                            <option value="4">Team-04</option>
                            <option value="5">Team-05</option>
                            <option value="5">Team-06</option>
                        </select>
                    </div>
                </div>
                <div class="hr-line-dashed1"></div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l1">	
                    <div class="form-group refaral-form">
                        <label>PIC</label>
                        <small>Sumit</small>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-r1">
                    <div class="form-group refaral-form">
                        <label>Region</label>
                        <small>R1</small>
                    </div>
                </div>
                <div class="hr-line-dashed1"></div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l1">
                    <div class="form-group refaral-form">
                        <label>Area</label>
                        <small>Area A</small>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>

    </div>







    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content rro-performance">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="bg-g text-center1" rowspan="2">Month-To-Date (MTD)</th>
                                        <th colspan="2" class="text-center1">Up-sell / Cross-sell</th>
                                        <th colspan="2" class="text-center1">Payment Reminder </th>
                                        <th colspan="2" class="text-center1">Servicing and Relationship Calls</th>
                                        <th colspan="2" class="text-center1">Total</th>
                                    </tr>
                                    <tr>

                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>Tasks assigned this month</td>
                                        <td class="text-center1">400</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">800</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks overdue</td>
                                        <td class="text-center1">320</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">720</td>
                                        <td class="text-center1">23%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Newly Assigned</td>
                                        <td class="text-center1">400</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">800</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks created by RRO (MTD)</td>
                                        <td class="text-center1">480</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">880</td>
                                        <td class="text-center1">28%</td>
                                    </tr>
                                    <tr class="gradeD">
                                        <td>Total tasks for current month</td>
                                        <td class="text-center1">1600</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">720</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">880</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">3200</td>
                                        <td class="text-center1">100%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Successful</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">240</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">640</td>
                                        <td class="text-center1">20%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Closed (Successful + Failed)</td>
                                        <td class="text-center1">320</td>
                                        <td class="text-center1">56%</td>
                                        <td class="text-center1">400</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">400</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">1120</td>
                                        <td class="text-center1">35%</td>
                                    </tr>
                                    <tr class="gradez">
                                        <td>Total no. of activities</td>
                                        <td class="text-center1" colspan="2">800</td>
                                        <td class="text-center1" colspan="2">640</td>
                                        <td class="text-center1" colspan="2">800</td>
                                        <td class="text-center1" colspan="2">2880</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="row">
                <h4 class="heading-tip">Week-To-Date (WTD)</h4>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Up-sell / Cross-sell</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_01.svg" class="img-t" alt="Up-sell-Cross-sell-r" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Loan Maintenance</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_02.svg" class="img-t" alt="Loan Maintenance" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Relationship Calls</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_03.svg" class="img-t" alt="Relationship Calls" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Servicing and Relationship Calls</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_04.svg" class="img-t" alt="Relationship Calls" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

