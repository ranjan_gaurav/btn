<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Internal Referrals</h2></div>
</div>
<form id="ref_form" method="post" class="wizard-big">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="lead-forms-section">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Referral information</h5>
                        </div>
                        <div class="ibox-content defulat-height">
                            <div class="form-group lead-forms refaral-id1">
                                <label>Referrer ID</label>
                                <input type="text" value="1" name="sales_staff_id" disabled="" placeholder="EM-005" class="form-control required"> 
                            </div>
                            <div class="form-group lead-forms refer_to">
                                <label>Refer To</label>
                                <select name="refer_to" id="refer_to" class="select-ui">
                                    <option value="0">Select</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Retail">Retail</option>
                                    <option value="Corporate">Corporate</option>
                                </select>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Lead Info</h5>
                        </div>
                        <div class="ibox-content defulat-height">
                            <div class="col-sm-6 padding-off-left">
                                <div class="form-group lead-forms1">
                                    <label class="red-high">Lead Source*</label>
                                    <select class="select-ui">
                                        <option value="Source 1">Source 1</option>
                                        <option value="Source 2">Source 2</option>
                                        <option value="Source 3">Source 3</option>
                                    </select>
                                </div>
                                <div class="form-group lead-forms1">
                                    <label class="red-high">Lead Status</label>
                                    <select class="select-ui" name="status">
                                        <option value="">Status</option>
                                        <option value="Unallocated">Unallocated</option>
                                        <option value="Allocated">Allocated</option>
                                        <option value="Engaged">Engaged</option>
                                        <option value="Application Ongoing">Application Ongoing</option>
                                        <option value="Pending Approval">Pending Approval</option>
                                        <option value="Onboarding">Onboarding</option>
                                        <option value="Withdrawn">Withdrawn</option>
                                        <option value="Rejected">Rejected</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 padding-off-right">

                                <div class="form-group lead-forms1">
                                    <label>Next schedule visit</label>
                                    <div class="date-select">
                                        <div class="date-dd">
                                            <select name="date" class="select-ui">
                                                <option value="">DD</option>
                                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="date-mm">
                                            <select name="month" class="select-ui">
                                                <option value="">MM</option>
                                                <option value="">January</option>
                                                <option value="">February</option>
                                                <option value="">March</option>
                                                <option value="">April</option>
                                                <option value="">May</option>
                                                <option value="">June</option>
                                                <option value="">July</option>
                                                <option value="">August</option>
                                                <option value="">September</option>
                                                <option value="">October</option>
                                                <option value="">November</option>
                                                <option value="">December</option>

                                            </select>
                                        </div>
                                        <div class="date-yy">
                                            <select name="year" class="select-ui">
                                                <option value="">YYYY</option>
                                                <?php for ($i = 2016; $i <= 2020; $i++) { ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="refral-section">
                    <div class="col-lg-6 col-sm-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Company Profile</h5>
                            </div>
                            <div class="ibox-content refaral-h">
                                <div class="form-group refaral-form">
                                    <label class="red-high">Name*</label>
                                    <input type="text" value="<?php echo ($this->input->post('client_name')) ? $this->input->post('client_name') : ''; ?>" name="lead_name" placeholder="Enter your Name" class="form-control required"> 
                                </div>
                                <div class="form-group refaral-form">
                                    <label>Industry</label>
                                    <select class="select-ui" name="industry">
                                        <option value="">Select</option>
                                        <option value="Restaurant">Restaurant</option>
                                        <option value="F&B">F&B</option>
                                        <option value="FMCG">FMCG</option>
                                        <option value="Autoparts">Autoparts</option>
                                        <option value="Retails">Retails</option>
                                        <option value="Tourism">Tourism</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group refaral-form">
                                    <label class="red-high">Address*</label>
                                    <input type="text" value="" name="address" placeholder="Enter your Address" class="form-control required"> 
                                </div>
                                <div class="form-group refaral-form">
                                    <label>Area</label>
                                    <select class="select-ui" name="area">
                                        <option value="Area 1">Area 1</option>
                                        <option value="Area 2">Area 2</option>
                                        <option value="Area 3">Area 3</option>
                                        <option value="Area 4">Area 4</option>
                                        <option value="Area 5">Area 5</option>
                                        <option value="Area 6">Area 6</option>
                                        <option value="Area 7">Area 7</option>
                                        <option value="Area 8">Area 8</option>
                                    </select>
                                </div>
                                <div class="form-group refaral-form">
                                    <label>Region</label>
                                    <select class="select-ui" name="region">
                                        <option value="Region 1">Region 1</option>
                                        <option value="Region 2">Region 2</option>
                                        <option value="Region 3">Region 3</option>
                                        <option value="Region 4">Region 4</option>
                                    </select>
                                </div>
                                <div class="form-group refaral-form">
                                    <label class="red-high">Phone No*</label>
                                    <input type="phone" value="" name="phone" placeholder="Enter your Phone No" class="form-control required"> 
                                </div>
                                <div class="form-group refaral-form">
                                    <label class="red-high">Email*</label>
                                    <input type="email" value="" name="email" placeholder="Enter your Email" class="form-control required"> 
                                </div>
                                <div class="clearfix">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Product Interests</h5>
                            </div>
                            <div class="ibox-content refaral-h">
                                <div class="form-group refaral-form">
                                    <label class="">Product Category</label> 
                                    <select id = "ddl" class="select-ui" name='product1_type'>
                                        <option value=''>Select Product</option>
                                        <option value="Funding">Funding</option>
                                        <option value="Lending">Lending</option>

                                    </select>
                                </div>
                                <div class="form-group refaral-form">
                                    <label class="">Product Interest</label> 
                                    <select class="select-ui" name='product1_name'>
                                        <option value=''>Product Interest</option>
                                        <option value="term_loan">Term Loan</option>
                                        <option value="term_loan_bundle">Term Loan Bundle</option>
                                        <option value="od_facility">OD Facility</option>
                                        <option value="od_facility_bundle">OD Facility Bundle</option>
                                        <option value="casa">CASA</option>
                                        <option value="casa_bundle">CASA Bundle</option>
                                        <option value="td">TD</option>
                                    </select>
                                    <!--<a href="<?php //echo base_url('assets/support/sales.pdf');        ?>" target="_blank">Details</a>-->
                                </div>
                                <div class="form-group refaral-form">
                                    <label>Nominal Amount</label>
                                    <input type="text" placeholder="" class="form-control" name='product1_amount'> 
                                </div>
                                <div class="form-group refaral-form">
                                    <label class="">Status</label> 
                                    <select class="select-ui" name='status_new'>
                                        <option value="" selected="selected">Select Status</option>
                                        <option value="Interested">Interested</option>
                                        <option value="Application prep">Application prep</option>
                                        <option value="Accepted by CF">Accepted by CF</option>
                                        <option value="OD Facility Bundle">OD Facility Bundle</option>
                                        <option value="Approved">Approved</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Appealed">Appealed</option>
                                        <option value="Withdrawn Pre-Application">Withdrawn Pre-Application</option>
                                        <option value="Withdrawn Post-Application">Withdrawn Post-Application</option>
                                        <option value="Referred">Referred</option>
                                        <option value="Disbursed">Disbursed</option>
                                    </select>
                                </div>
                                <div class="clearfix">&nbsp;</div>

                            </div>
                        </div>
                    </div>
                    <div class="lead-details">
                        <span>Comment:</span>
                        <textarea name="comment" width="100%" class="form-control" cols="20" rows="2"></textarea>
                    </div>
                    <div class="lead-btns">
                        <button class="btn btn-w-m btn-primary indi-refer" type="submit">Done</button>
                         <button class="btn btn-w-m btn-info indi-refer" type="Button" id="b1" onclick = "myFunction()">Cancel</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>

<script>
function myFunction() {
    document.getElementById("ref_form").reset();
   // bbn();
}
</script>