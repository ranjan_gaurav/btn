<div class="card style-transparent">
    <div class="card-body">
        <div class="row">
            <div class = "container">
                <div class="doc-registration-form">
                    <div class="login_wrapper wrapper">
                        <form action="" method="post" name="Login_Form" class="form-signin form floating-label">       
                            <h3 class="form-signin-heading">Reset Password : </h3>
                            <hr class="colorgraph"><br>
                            <?php if (@$error): ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="password" class="form-control" id="new_password" name="new_password" required/>
                                <label for="new_password">New Password</label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="cnf_password" name="cnf_password" required/>
                                <label for="cnf_password">Confirm Password</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Send" type="Submit">Send</button>  			
                        </form>			
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
