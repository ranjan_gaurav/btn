<?php

/*

 */

class Users extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function is_admin($id) {
        if (@$this->userCheck($id)->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    function userCheck($id) {
        return $this->user_model->user_by_id($id);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('signin'));
    }

//Hidden Methods not allowed by url request

    function _member_area() {
        if (!$this->_is_logged_in()) {
            redirect('signin');
        }
    }

    function _is_logged_in() {
        if ($this->session->userdata('logged_in')) {
            return true;
        } else {
            return false;
        }
    }

    function userdata() {
        if ($this->_is_logged_in()) {
            return $this->user_model->user_by_id($this->session->userdata('userid'));
        } else {
            return false;
        }
    }

    function _is_admin() {
        if (@$this->userdata()->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    function signin() {
        if ($this->_is_logged_in()) {
            $rolesArray = array('1' => 'head', '2' => 'rrm/pipeline', '3' => 'home', '4' => 'hunter', '5' => 'rroleader/pipeline');
            $userID = @$this->session->userdata('userid');
            $roleId = @$this->userCheck($userID)->role;
            redirect(base_url($rolesArray[$roleId]));
        }
        $data['meta_title'] = "Bank BRI Login";
        $data['meta_keyword'] = "Bank BRI Login";
        $data['meta_description'] = "Bank BRI Login";
        if ($_POST) {
            $user_email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $userdata = $this->user_model->validate($user_email, md5($password));
            if ($userdata) {
                if ($userdata->status == 0) {
                    $data['error'] = "Not validated!";
                    $data['main_content'] = 'signin';
                    $this->setData($data);
                } else {
                    if ($this->userCheck($userdata->id)) {
                        $data['userid'] = $userdata->id;
                        $data['role'] = @$this->userCheck($userdata->id)->role;
                        $data['logged_in'] = true;
                        $this->session->set_userdata($data);
                        $roleArray = array('1' => 'head', '2' => 'rrm', '3' => 'home', '4' => 'hunter', '5' => 'rroleader');
                        $roleID = @$this->userCheck($userdata->id)->role;
                        
                    
                        
                        redirect(base_url($roleArray[$roleID]));
                    } else {
                        $data['error'] = "Not validated!";
                        $data['main_content'] = 'signin';
                        $this->setData($data);
                    }
                }
            } else {
                $data['error'] = "Invalid Credentails!";
                $this->setData($data);
            }
            return;
        }
        $data['main_content'] = 'signin';
        $this->setData($data);
    }

    function forgot_password() {
        $data['meta_title'] = "Forgot Password";
        $data['meta_keyword'] = "Forgot Password";
        $data['meta_description'] = "Forgot Password";
        $data['main_content'] = 'forgot_password';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $this->load->library('email');
            $key = $this->user_model->getUserKey($this->input->post('user_email'));
            $message = 'Please follow the link to reset your password:' . base_url() . 'admin/users/reset_password?key=' . $key;
            $this->email->from(ADMIN_EMAIL, 'Admin');
            $this->email->to($this->input->post('user_email'));
            $this->email->subject('Login Password Reset');
            $this->email->message($message);
            $this->email->send();
            $this->session->set_flashdata('message', 'Mail Sent Successfully! Please check ');
        }
        $this->setData($data);
    }

}

?>