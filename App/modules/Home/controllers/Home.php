<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Home extends Front_Controller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library("session");
        $this->load->model('home_model', 'home');
    }

    /*
     * Home Dashbord
     */

    public function index($userID = '5') {
    	
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/canvasjs.min.js',
            'assets/js/user_profile.js',
            'assets/js/hunterCharts.js',
            'assets/js/canvasCharts.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['meta_title'] = "Bank BRI";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['main_content'] = 'index';
        $data['info'] = $this->home->getHunterInfo();
        $data['result'] = $this->home->getHunterData();
        $users = array('first_name' => 'WILLIAM WINATA', 'user_pic' => 'i7.jpg', 'username' => 'william');
        $data['users'] = (object) $users;
        // $data['users']=$this->home->GetUserInformation($userID);
        $this->setData($data);
    }

    /*
     * Home Performance Dashbord : Team View
     */

    public function performance() {

        error_reporting(0);
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/jasny/jasny-bootstrap.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/daterangepicker/daterangepicker.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/performance.js',
            'assets/js/user_profile.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/datapicker/datepicker3.css',
            'assets/css/plugins/daterangepicker/daterangepicker-bs3.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Home Performance";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['result'] = $this->home->getHunterList();
        $data['alerts'] = $this->home->getAlerts();
        // $data['users'] = $this->home->getUserDetails();
        $this->setData($data);
    }

    /*
     * Home Sales Activity Summary
     */

    public function summary() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/lead_directory.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/barChart.jquery.js',
            'assets/js/canvasjs.min.js',
            //'assets/js/hunterCharts.js',
            'assets/js/canvasCharts.js',
            'assets/js/summary.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/barchart.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Hunter Sales Activity Summary";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    /*
     * Convassing Area Allocator
     */

    public function area_allocator() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assetsjs/plugins/iCheck/icheck.min.js',
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/area_allocator.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/codemirror/codemirror.css',
            'assets/css/plugins/codemirror/ambiance.css',
            'assets/css/plugins/steps/jquery.steps.css',
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules('region', 'Region', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $insert = $this->home->insertAllocatorData();
            if ($insert) {
                $this->session->set_flashdata('message', 'Area has been allocated successfully');
            } else {
                $data['error'] = 'Something wrong Happen with database;';
            }
        }
        $data['result'] = $this->home->GetAllocatorsList();
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Home Area Allocator";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    public function performance_summary() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/jasny/jasny-bootstrap.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/daterangepicker/daterangepicker.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/performance.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/datapicker/datepicker3.css',
            'assets/css/plugins/daterangepicker/daterangepicker-bs3.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Home Performance";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['result'] = $this->home->getHunterList();
        $data['alerts'] = $this->home->getAlerts();
        $this->setData($data);
    }

    public function users_info() {
        $userID = $this->input->post('id');
        $data['users'] = $this->home->getUserDetails($userID);
        $this->load->view('profile_info', $data);
    }

}

?>