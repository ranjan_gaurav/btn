<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10 col-sm-10 col-md-10"><h2>Canvassing Area allocator</h2></div>
    <div class="col-lg-2 col-sm-2 col-md-2"><button type="button" class="btn btn-w-m btn-info pull-right" data-toggle="modal" data-target="#areaAllocator">Add</button></div>
</div>
<?php if ($this->session->flashdata('message')) { ?>
   
    <div class="panel panel-success" id="flash-message">
        <div class="panel-heading">
            Success Panel
        </div>
        <div class="panel-body">
            <p><?php echo $this->session->flashdata('message'); ?></p>
        </div>
    </div>
<?php } ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content-new">

                    <div class="clearfix"></div>
                    <div class="content-scroll">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Region</th>
                                            <th>Area</th>
                                            <th>Location</th>
                                            <th>Hunter Name</th>
                                            <th>Assignment Status</th>
                                            <th>Completion Status</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="gradeX">
                                            <td>1</td>
                                            <td>R1</td>
                                            <td>S&D-001</td>
                                            <td>Thamrin Kav. 28-30, Jakarta 10350</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Assigned</td>
                                            <td class="center">In progression</td>


                                        </tr>
                                        <tr class="gradeC">
                                            <td>2</td>
                                            <td>R2</td>
                                            <td>S&D-002</td>
                                            <td>Kawasan Wisata Nusa Dua BTDC, Nusa Dua, Badung</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Assigned</td>
                                            <td class="center">Completed</td>

                                        </tr>
                                        <tr class="gradeA">
                                            <td>3</td>
                                            <td>R3</td>
                                            <td>S&D-003</td>
                                            <td>Kav., Jl. M.H. Thamrin No.15, Gondangdia, Jakarta</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Unassigned</td>
                                            <td class="center">Completed</td>

                                        </tr>
                                        <tr class="gradeA 52cecc">
                                            <td>4</td>
                                            <td>R4</td>
                                            <td>S&D-004</td>
                                            <td>Kawasan Wisata Nusa Dua BTDC, Nusa Dua, Badung</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Assigned</td>
                                            <td class="center">In progression</td>

                                        </tr>
                                        <tr class="gradeC">
                                            <td>5</td>
                                            <td>R5</td>
                                            <td>S&D-005</td>
                                            <td>Kav., Jl. M.H. Thamrin No.15, Gondangdia, Jakarta</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Unassigned</td>
                                            <td class="center">Completed</td>

                                        </tr>
                                        <tr class="gradeA">
                                            <td>6</td>
                                            <td>R6</td>
                                            <td>S&D-006</td>
                                            <td>Thamrin Kav. 28-30, Jakarta 10350</td>
                                            <td class="center">
                                                <select class="form-control with-select" id='area_select' name='area' onchange='SearchResult();'>
                                                    <option value='' selected>Luthfi Zarkasyi</option>
                                                    <option value='Area 1'>Minggu Swanto</option>
                                                    <option value='Area 2'>Anton Maraci</option>
                                                </select>
                                            </td>
                                            <td class="center">Unassigned</td>
                                            <td class="center">In progression</td>

                                        </tr>
                                        <?php
                                        $i = 7;
                                        foreach ($result as $allocatorListing) {
                                            ?>
                                            <tr class="gradeA">
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $allocatorListing->region;?></td>
                                                <td><?php echo $allocatorListing->area;?></td>
                                                <td><?php echo $allocatorListing->address;?></td>
                                                <td class="center">
                                                    <select class="form-control with-select" id='area_select'>
                                                        <option value='' selected><?php echo $allocatorListing->hunter_name;?></option>
                                                    </select>
                                                </td>
                                                <td class="center"><?php echo $allocatorListing->assigned_status;?></td>
                                                <td class="center"><?php echo $allocatorListing->completion_status;?></td>

                                            </tr>
                                            <?php $i++;
                                        } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                </div>

            </div>
        </div>
    </div>
</div>




<div class="modal inmodal" id="areaAllocator" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm2">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Canvas Area Allocator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="allocatorForm">
                    <div class="col-sm-12 col-md-12">

                        <div class="height-min">
                            <div class="follow-up">Area Allocator</div>
                            <div class="form-group lead-forms2">
                                <label class="red-high">Region:</label>
                                <select class="selectpicker required" name="region">
                                    <option value="">Select</option>
                                    <option value="R1">R1</option>
                                    <option value="R2">R2</option>
                                    <option value="R3">R3</option>
                                    <option value="R4">R4</option>
                                    <option value="R5">R5</option>
                                    <option value="R6">R6</option>
                                </select>
                            </div>
                            <div class="form-group lead-forms2">
                                <label class="red-high">Area:</label>
                                <select class="selectpicker" name="area">
                                    <option value="">Select</option>
                                    <option value="S&D-001">S&D-001</option>
                                    <option value="S&D-002">S&D-002</option>
                                    <option value="S&D-003">S&D-003</option>
                                    <option value="S&D-004">S&D-004</option>

                                </select>
                            </div>
                            <div class="form-group lead-forms2">
                                <label>Address*</label>
                                <input type="text" placeholder="Enter Name" class="form-control required" name="address" value="Kav., Jl. M.H. Thamrin No.15, Gondangdia, Jakarta">
                            </div>
                            <div class="form-group lead-forms2">
                                <label class="">Hunter Name:</label>
                                <select class="selectpicker" name="hunter_name">
                                    <option value="">Select</option>
                                    <option value="Luthfi Zarkasyi" selected="">Luthfi Zarkasyi</option>
                                    <option value="Minggu Swanto">Minggu Swanto</option>
                                    <option value="Anton Maraci">Anton Maraci</option>

                                </select>
                            </div>
                            <div class="form-group lead-forms2">
                                <label class="">Assignment Status:</label>
                                <select class="selectpicker" name="assigned_status">
                                    <option value="">Select</option>
                                    <option value="Assigned" selected="">Assigned</option>
                                    <option value="Unassigned">Unassigned</option>
                                </select>
                            </div>
                            <div class="form-group lead-forms2">
                                <label class="">Completion Status:</label>
                                <select class="selectpicker" name="completion_status">
                                    <option value="">Select</option>
                                    <option value="Completed" selected="">Completed</option>
                                    <option value="In progression">In progression</option>
                                </select>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>

                    </div>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-w-m btn-primary pull-right btn-m-top">Save</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>

    </div>
</div>