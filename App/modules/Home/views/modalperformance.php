<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10 col-sm-10 col-md-10"><h2 class="exp-icons-text">Leader Performance Dashboard</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            
            <div class="profile-information" id="profile-information12"> 
                <?php $this->load->view('profile_info'); ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="information-section">
                <div class="heading-l">Last Month Performance </div>
                <div class="information-section-area">
                    <div class="information-section-content">Ranking (Percentile)</div>
                    <div class="information-section-content1">30%</div>
                </div>
                <div class="information-section-area">
                    <div class="information-section-content">Incentive Score</div>
                    <div class="information-section-content1">15</div>
                </div>
                <div class="information-section-area">
                    <div class="information-section-content">Bonus Earned (*)(IDR '000)</div>
                    <div class="information-section-content1">1,000</div>
                </div>

            </div>

        </div>
    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>KPIs</h5>
                </div>
                <div class="ibox-content">
                    <div class="financial_kpi">
                        <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/hunter_performance_dash.svg" height="260" class="img-t" alt="financial_kpi">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-4 chart-padding-middle">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Incentive Scheme</h5>
                </div>
                <div class="ibox-content">
                    <div class="process-indicators15">
                        <div id="content-md9" class="content">
                            <div class="col-lg-6 col-sm-6 chart-padding-right">
                                <div class="grap-content">
                                    <div class="ibox-title">
                                        <h5>Sold Loan Bundle to NTB</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="loanbunddlechart">
                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Sold_Loan_Bundle_to_NTB.svg" class="img-t" alt="financial_kpi" height="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 chart-padding-left">
                                <div class="grap-content">
                                    <div class="ibox-title">
                                        <h5>Sold Loan Only to NTB</h5>

                                    </div>
                                    <div class="ibox-content">
                                        <div class="ntbwithLoanChart">
                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Sold_Loan_Only_to_NTB.svg" class="img-t" alt="financial_kpi" height="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 chart-padding-right">
                                <div class="grap-content">
                                    <div class="ibox-title">
                                        <h5>Sold CASA</h5>

                                    </div>
                                    <div class="ibox-content">
                                        <div class="refferalChart">
                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Sold_CASA.svg" class="img-t" alt="financial_kpi" height="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 chart-padding-left">
                                <div class="grap-content">
                                    <div class="ibox-title">
                                        <h5>Successful Referrals</h5>

                                    </div>
                                    <div class="ibox-content">
                                        <div class="refferalChart">
                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Successful_Referrals.svg" class="img-t" alt="financial_kpi" height="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 padding-none-all">
                                <div class="grap-content">
                                    <div class="ibox-title">
                                        <h5>Sold Loan to Non-Loan ETB Served by Funding RO/RM</h5>

                                    </div>
                                    <div class="ibox-content">
                                        <div class="ntbwithoutLoanChart">
                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/served_by_funding_RORM.svg" class="img-t" alt="financial_kpi" height="100">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Process Indicators</h5>
                </div>
                <div class="ibox-content">

                    <div class="process-indicators12">
                        <div class="content demo-yx">
                            <div class="table-responsive18">
                                <table class="table table-bordered">
                                     <thead>
                                    <tr>
                                        <th>Indicator</th>
                                        <th>This Week</th>
                                        
										<th>Month-to-Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lead Created</td>
                                        <td>3</td>
<!-- 										<td><div class="icon-arro"> -->
<!--                                                 <i class="fa fa-long-arrow-down" aria-hidden="true"></i> -->
<!--                                             </div> -->
<!--                                         </td> -->
										<td>7</td>
                                        
                                    </tr>
                                    <tr>
                                        <td>Visits</td>
                                        <td>12</td>
                                       <!--  <td>
										<div class="icon-arro">
                                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                                            </div>
										</td> -->
                                        <td>30</td>
                                    </tr>
                                    <tr>
                                        <td>Applications Accepted by CF</td>
                                        <td>3</td>
                                        <!-- <td><div class="icon-arro">
                                                <i class="fa fa-arrows-h" aria-hidden="true"></i>
                                            </div>
                                        </td> -->
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>No. of Appl. Approved (MTD)</td>
                                        <td>1</td>
                                       <!--  <td><div class="icon-arro">
                                                <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                            </div></td> -->
                                        <td>4</td>
                                    </tr>
                                </tbody>
                            </table>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Indicator</th>
                                            <th>Last Month</th>
                                            <!-- <th>Trend </th>-->
                                            <th>Last Three Months</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Lead Conversion Rate</td>
                                            <td>12%</td>
                                            <!--<td>
                                                <div class="icon-arro">
                                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                                                </div>
                                            </td> -->
                                            <td>8%</td>
                                        </tr>
                                        <tr>
                                            <td>Avg. Visits Per Lead</td>
                                            <td>3</td>
                                            <!--<td>
                                                <div class="icon-arro">
                                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                                                </div>
                                            </td> -->
                                            <td>6</td>
                                        </tr>
                                        <tr>
                                            <td> TAT to Acceptance by CF</td>
                                            <td>8</td>
                                            <!--<td>
                                                <div class="icon-arro">
                                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                                                </div>
                                            </td> -->
                                            <td>10</td>

                                        </tr>
                                        <tr>
                                            <td>TAT from Acceptance by CF to Disbursement</td>
                                            <td>4</td>
                                            <!--<td>
                                                <div class="icon-arro">
                                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                                                </div>
                                            </td> -->
                                            <td>3</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>
<div class="flot-chart" style="display:none;">
    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
</div>