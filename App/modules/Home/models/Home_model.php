<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Home_model extends CI_Model {

    var $table = "hunter";
    var $data_table = "hunter_data";
    var $alert_table = "alerts";
    var $visit_table = "visits";
    var $allocator_table="area_allocator";

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /*
     * Get Hunder information from the hunter table
     */

    public function getHunterInfo() {
        $Id = '1';
        $this->db->where('h.hunter_id', $Id);
        $this->db->select('h.*,SUM(`d`.`lead_creation`) as sum_lead_creation,SUM(`d`.`lead_conversion`) as sum_lead_conversion,SUM(`d`.`visits`) as sum_visits,SUM(`d`.`visit_per_lead`) as sum_visit_per_lead,SUM(`d`.`approx_approval_rate`) as sum_approx_approval_rate,SUM(`d`.`tat_bt_created_to_sub`) as sum_tat_bt_created_to_sub,SUM(`d`.`tat_bt_sub_to_onboard`) as sum_tat_bt_sub_to_onboard');
        $this->db->from($this->table . ' h');
        $this->db->group_by('h.hunter_id');
        $this->db->join($this->data_table . ' d', 'd.hunter_id=h.hunter_id');
        $query = $this->db->get();
        
       
        return $query->row();
    }

    /*
     * Get Hunder Data based on Hunter Id and Month Basis from the hunter_data table
     */

    public function getHunterData() {
        $Id = '1';
        $this->db->where('hunter_id', $Id);
        $this->db->group_by('MONTH(creation_date)');
        $query = $this->db->get($this->data_table);
        return $query->result();
    }

    public function getHunterList() {
        $this->db->select('h.*,h.userid ,d.visit_per_lead,d.tat_bt_created_to_sub,SUM(`d`.`ntb`) as sum_ntb,SUM(`d`.`ntb_with_bundle`) as sum_ntb_with_bundle,SUM(`d`.`ntb_with_loan_only`) as sum_ntb_with_loan_only,SUM(`d`.`ntb_without_loan`) as sum_ntb_without_loan,SUM(`d`.`successful_refererrals`) as sum_successful_refererrals,SUM(`d`.`lead_creation`) as sum_lead_creation,SUM(`d`.`lead_conversion`) as sum_lead_conversion,SUM(`d`.`visits`) as sum_visits,SUM(`d`.`visit_per_lead`) as sum_visit_per_lead,SUM(`d`.`approx_approval_rate`) as sum_approx_approval_rate,SUM(`d`.`tat_bt_created_to_sub`) as sum_tat_bt_created_to_sub,SUM(`d`.`tat_bt_sub_to_onboard`) as sum_tat_bt_sub_to_onboard');
        $this->db->from($this->table . ' h');
        $this->db->group_by('h.hunter_id');
        $this->db->join($this->data_table . ' d', 'd.hunter_id=h.hunter_id');
        $this->db->join('users' . ' u', 'h.hunter_id=u.id','left');
        $query = $this->db->get();
    //  echo  $this->db->last_query(); die();
        return $query->result();
    }

    public function getAlerts() {
        $this->db->from($this->alert_table);
        $query = $this->db->get();
        return $query->result();
    }

    public function getVisits() {
        $this->db->from($this->visit_table);
        $query = $this->db->get();
        return $query->result();
    }

    public function GetUserInformation($userID) {
        $this->db->where('id',$userID);
        $this->db->from('users');
        $query = $this->db->get();
       // echo $this->db->last_query(); die();
        return $query->row();
    }
    
    public function getUserDetails($userID)
    {
    	 $this->db->where('id',$userID);
        $this->db->from('users');
        $query = $this->db->get();
      //  echo $this->db->last_query(); die();
        return $query->row();
    	
    }
    
    public function insertAllocatorData(){
        $data=$this->input->post();
        $insert=$this->db->insert($this->allocator_table,$data);
        if($insert){
            return true;
        }else{
            return false;
        }
    }
    
    public function GetAllocatorsList(){
        $this->db->from($this->allocator_table);
        $list=$this->db->get();
        return $list->result();
    }
    
  

}

?>