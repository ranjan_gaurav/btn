<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav <yadav.sanjay@orangemantra.in>
 * Date: 15/01/2016
 * Time: 11:28 AM
 */
class Client_model extends CI_Model {

    var $client_table = "app_client";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get User data
     */
    public function getAllClients($ID = null) {
        if ($ID != null) {
            $this->db->where('client_id', $ID);
        }
        $this->db->select('*');
        $this->db->from($this->client_table);
        $result = $this->db->get();
        return $result->result();
    }
    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get User data
     */
    public function getAllClientsAfterId($ID = null) {
        if ($ID != null) {
            $this->db->where('client_id >', $ID);
        }
        $this->db->select('*');
        $this->db->from($this->client_table);
        $result = $this->db->get();
        return $result->result();
    }

}

?>