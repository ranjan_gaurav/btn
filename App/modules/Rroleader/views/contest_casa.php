<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Opportunity Contest Dashboard – CASA Balances</h2></div>
</div>
<div class="row"><div class="top-space">
<div class="col-lg-12 col-sm-12 col-md-12">
<div class="user-name1"><b>Customer <?php echo $client->name; ?></b></div>
</div>
        <div class="col-lg-7 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5>Historical CASA Balance</h5>               
                </div>
                <div class="ibox-content">
				<div class="graph55">
				<img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Historical_CASA_Balance.svg" class="img-t" alt="financial_kpi">
				</div>
                    <!-- <div id="morris-one-line-chart"></div>
                    <div class="trans-p-w"><span>Transactions Per Week</span></div>
                    <div class="trans-p-w"><span>Weekly Transaction Amount</span></div>-->
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

        <div class="col-lg-5 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5>Outflow to Same Name Accounts</h5>               
                </div>
                <div class="ibox-content contest_casa">
				<div class="content" id="content-md1">
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Bank</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>22-04-2016</td>
                                <td>50,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>22-03-2016</td>
                                <td>60,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>23-04-2016</td>
                                <td>70,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>21-05-2016</td>
                                <td>80,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>20-06-2016</td>
                                <td>90,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>22-07-2016</td>
                                <td>50,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr><tr>
                                <td>22-04-2016</td>
                                <td>60,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>21-05-2016</td>
                                <td>70,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>20-06-2016</td>
                                <td>80,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>15-04-2016</td>
                                <td>90,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>17-04-2016</td>
                                <td>10,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                            <tr>
                                <td>24-03-2016</td>
                                <td>20,000,000</td>
                                <td>Bank - Mandiri</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
			</div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <a href="<?php echo base_url('rroleader/profile');?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp; &nbsp; Go Back</a>
    </div>
</div>

