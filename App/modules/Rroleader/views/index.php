<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2 class="heading-gap">RRO Leader Performance Dashboard</h2></div>
<!--  	<div class="col-lg-2"><a href="<?php //echo base_url('rroleader/performance');?>" class="btn btn-w-m btn-info pull-right">Expansion</a></div> -->
   
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <div class="view-btn">
                 <div class="form-group">
                    <label>View :</label>
                    <select name="Company_Name" class="selectpicker required" id="user_dropdown" onChange="changeProfileInformation(this.value);">
                        <option value="11">ANTON MARACI (TL)</option>
                        <option value="12">Evans</option>
                        <option value="13">Lista Permatasari</option>
                        <option value="10">Luthfi Zarkasyi</option>
                        <option value="6">Minggu</option>
                    </select>
                   
                </div>
            </div>
            <div class="profile-information" id="profile-information">
                <?php $this->load->view('profile_info');?>
            </div>

        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="information-section">
                <div class="heading-l">Last Month Performance </div>
                <div class="information-section-area">
                    <div class="information-section-content">Incentive Score</div>
                    <div class="information-section-content1">30</div>
                </div>
                <div class="information-section-area">
                    <div class="information-section-content">Ranking (Percentile)</div>
                    <div class="information-section-content1">15%</div>
                </div>
                
                <div class="information-section-area">
                    <div class="information-section-content">Bonus Earned (*)(IDR '000)</div>
                    <div class="information-section-content1">1,000</div>
                </div>
            </div>

        </div>

    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
<div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 chart-padding-middle">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Financial KPIs</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Loan Balance Growth</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="loanbunddlechart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Loan_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Term Loan Re-Instatement</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Term_Loan_Re-instatement.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>CASA Balance Growth </h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/CASA_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Average CASA Transactions/Customer</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Average_CASA_Transactions-customer.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Portfolio NPL</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Portfolio_NPL.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Cross selling points</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/rro_leader.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</div>
<div id="myModal" class="modal">
	<div id="modal-content" class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
		</div>
		<div class="modal-body">
			<input type="hidden" id="P_reqesturl"
				value="<?php echo base_url('rroleader/user_info'); ?>" /> 
            <?php
												$this->load->view ( 'modalperformance' );
												?>
        </div>
	</div>
</div>
