<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Performance Dashboard</h2></div>
</div>
<?php
$hunterInfo = getUserInfo(@$this->session->userdata('userid'));
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <!--<div class="view-btn">
                <div class="form-group">
                    <div class="view-name">View :<span><?php echo $hunterInfo->first_name; ?></span></div>
                </div>
            </div> -->
            <div class="profile-image">
                <img src="<?php echo base_url(); ?>/assets/img/<?php echo $hunterInfo->user_pic; ?>" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            <?php echo $hunterInfo->first_name; ?>
                        </h2>
                        <h4>+62 2345 2345</h4>
                        <h4><?php echo $hunterInfo->username; ?>@btn.co.id</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">

        </div>

    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content rro-performance">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="bg-g">&nbsp;</th>
                                        <th colspan="2" class="text-center1">Up-sell / Cross-sell</th>
                                        <th colspan="2" class="text-center1">Loan Maintenance</th>
                                        <th colspan="2" class="text-center1">Relationship Calls</th>
                                        <th colspan="2" class="text-center1">Total</th>
                                    </tr>
									<tr>
                                        <th class="bg-g">&nbsp;</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No.</th>
                                        <th class="text-center1">%</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>Tasks assigned this month</td>
                                        <td class="text-center1">40</td>
                                        <td class="text-center1">25%</td>
                                        <td class="text-center1">16</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">24</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">80</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks overdue</td>
                                        <td class="text-center1">32</td>
                                        <td class="text-center1">20%</td>
                                        <td class="text-center1">16</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">24</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">72</td>
                                        <td class="text-center1">23%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Newly Assigned</td>
                                        <td class="text-center1">40</td>
                                        <td class="text-center1">25%</td>
                                        <td class="text-center1">16</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">24</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">80</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks created by RRO (MTD)</td>
                                        <td class="text-center1">48</td>
                                        <td class="text-center1">30%</td>
                                        <td class="text-center1">24</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">16</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">88</td>
                                        <td class="text-center1">28%</td>
                                    </tr>
                                    <tr class="gradeD">
                                        <td>Total tasks for current month</td>
                                        <td class="text-center1">160</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">72</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">88</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">320</td>
                                        <td class="text-center1">100%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Successful</td>
                                        <td class="text-center1">48</td>
                                        <td class="text-center1">60%</td>
                                        <td class="text-center1">32</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">32</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">112</td>
                                        <td class="text-center1">78%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Closed (Successful + Failed)</td>
                                        <td class="text-center1">32</td>
                                        <td class="text-center1">40%</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">0</td>
                                        <td class="text-center1">32</td>
                                        <td class="text-center1">22%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-lg-12 col-sm-12 col-md-12">
		<div class="row">
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Up-sell / Cross-sell</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_04.svg" class="img-t" alt="Up-sell-Cross-sell-rl" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Loan Maintenance</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_05.svg" class="img-t" alt="Loan Maintenance" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-4 col-sm-4 col-md-4">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Relationship Calls</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_06.svg" class="img-t" alt="Relationship Calls" height="320">
            </div>
		  </div>
		  </div>
		 </div>
		</div>
		</div>
    </div>
</div>

