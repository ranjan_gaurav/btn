<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
	<div class="col-lg-8 col-md-6 col-sm-6">

	</div>
         <div class="col-lg-4 col-md-6 col-sm-6" id="remove2">
            <div class="information-section" id="information">
                <div class="heading-l">Last Month Performance </div>
                <div class="information-section-area">
                    <div class="information-section-content">Incentive Score</div>
                    <div class="information-section-content1">30</div>
                </div>
                <div class="information-section-area">
                    <div class="information-section-content">Ranking (Percentile)</div>
                    <div class="information-section-content1">15%</div>
                </div>
                
                <div class="information-section-area">
                    <div class="information-section-content">Bonus Earned (*)(IDR '000)</div>
                    <div class="information-section-content1">1,000</div>
                </div>
            </div>

        </div>
    </div>
    <?php
//     $array = array();
//     foreach ($result as $arrdata) :
//         array_push($array, $arrdata->ntb);
//     endforeach;
//     ?>
<!--     <script> 
//         var financialntb = [];
//         var ntbwithloadbundle = [];
//         var ntbwithloan = [];
//         var ntbwithoutloan = [];
//         var successfullrefferals = [];
//         var months = [];
// <?php //foreach ($result as $data) : ?>
            financialntb.push(['<?php //echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php //echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php //echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php //echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php //echo $data->successful_refererrals ?>']);
            months.push(['<?php //echo date('F', strtotime($data->creation_date)) ?>']);
// <?php //endforeach; ?>
    </script> -->
<div class="row" id="remove">
                                <div class="col-lg-12 col-sm-12 col-md-12 chart-padding-middle">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Financial KPIs</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Loan Balance Growth</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="loanbunddlechart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Loan_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Term Loan Re-Instatement</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Term_Loan_Re-instatement.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>CASA Balance Growth </h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/CASA_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Average CASA Transactions/Customer</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Average_CASA_Transactions-customer.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Portfolio NPL</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Portfolio_NPL.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Cross selling points</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Cross_selling_points.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
</div>