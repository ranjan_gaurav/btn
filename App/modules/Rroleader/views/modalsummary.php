<!--<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Portfolio Summary</h2></div>
</div>-->
                <div class="col-lg-5 col-sm-4 col-md-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title rrm-title">
                            <h5>Portfolio Breakdown</h5>
                        </div>
                        <div class="ibox-content summary-height">
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <div class="graph1">
                                        <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/summary/Breakdown-by-Product.png" class="img-t" alt="Risk Management">
                                        <span>Breakdown<br/> by Product</span>
                                    </div>
                                    <!-- <div id="chart1-1"></div>-->
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-4 padding-none">
                                    <div class="lagent-items">
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#79a2b3"></i>  Working Capital </p>      
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#ffc321"></i> Business Premises Loan </p>
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#f8941c"></i>  CASA/TD </p>
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#f36a3e"></i> Other Products </p>
                                    </div>
                                    <div class="lagent-items">
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#e2e2e2"></i> Working Capital </p>      
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#b2b2b2"></i> Business Premises Loan </p>
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#808080"></i> CASA/TD </p>
                                        <p class="font-bold"><i class="fa fa-square fa-1x" style="color:#4d4d4d"></i> Other Products </p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <div class="graph2">
                                        <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/summary/Breakdown-by-sector.png" class="img-t" alt="Risk Management">
                                        <span>Breakdown<br/> by Sector</span>
                                    </div>
                                    <!--   <div id="chart1-2"></div>-->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-sm-8 col-md-8 padding-l">
                    <div class="col-lg-4 col-sm-4 col-md-4 padding-l">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title rrm-title1">
                                <h5>Lending Balance History(Bn)</h5>
                            </div>
                            <div class="ibox-content text-center summary-height">
							
                                <div class="graph3">
                                    <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/summary/rro/Lending-Balance-History.png" class="img-t" alt="Risk Management">
                                </div>
                                <div class="targets">
                                    <div class="limits"><i class="fa fa-square fa-1x"></i> Limit</div>
                                    <div class="outstanding"><i class="fa fa-square fa-1x"></i> Outstanding</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4 padding-zero">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title rrm-title1">
                                <h5>Funding Balance History(Bn)</h5>
                            </div>
                            <div class="ibox-content text-center summary-height">
							
                                <div class="graph3">
                                    <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/summary/rro/Funding-Balance-History.png" class="img-t" alt="Risk Management">

                                </div>
                                <div class="targets3">
                                    <div class="limits"><i class="fa fa-square fa-1x"></i> TD</div>
                                    <div class="current-acc"><i class="fa fa-square fa-1x"></i> Savings Acct</div>
                                    <div class="outstanding"><i class="fa fa-square fa-1x"></i> Current Acct</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4 padding-r">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title rrm-title1">
                                <h5>Fee-Based Product</h5>
                            </div>
                            <div class="ibox-content text-center summary-height">
                                <div class="graph3">
                                    <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/summary/rro/Fee-based-product.png" class="img-t" alt="Risk Management">

                                </div>

                            </div>
                        </div>
                    </div>


                </div>

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title rrm-title">
                    <h5>Client Portfolio</h5>
                </div>
                <div class="ibox-content portfolio_table">
                    <table class="table table-striped text-center">
                        <thead>
                            <tr>
                                <th class="text-center navy-bg" style="vertical-align:middle;">Client</th>
                                <th class="text-center navy-bg">CASA <br>Balance(Mn)</th>
                                <th class="text-center navy-bg">TD <br>Balance(Mn)</th>
                                <th class="text-center red2-bg">Credit <br>Limit(Mn)</th>
                                <th class="text-center red2-bg">Credit <br>Outstanding(Mn)</th>
                                <th class="text-center red2-bg">Next <br>Pmt Date</th>
                                <th class="text-center red2-bg">Next <br>Pmt Amount(Mn)</th>
                                <th class="text-center red2-bg">Renewal <br>Date</th>
                                <th class="text-center red2-bg" style="vertical-align:middle;"> DPD Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($clients as $client) : ?>
                                <tr>
                                    <td><a href="<?php echo base_url('rroleader/profile/' . $client->client_id); ?>"><?php echo $client->name; ?></a></td>
                                    <td><?php echo $client->casa_balance; ?></td>
                                    <td><?php echo $client->td_balance; ?></td>
                                    <td><?php echo $client->credit_limit; ?></td>
                                    <td><?php echo $client->credit_outstanding; ?></td>
                                    <td><?php echo $client->next_pmt_date; ?></td>
                                    <td><?php echo $client->next_pmt_amount; ?></td>
                                    <td><?php $date=date_create($client->renewal_date);echo date_format($date,"m.d.Y"); ?></td>
                                    <td><?php echo $client->status; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

