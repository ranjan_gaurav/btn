<div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">

                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="bg-g text-center1" rowspan="2">Month-To-Date (MTD)</th>
                                        <th colspan="2" class="text-center1">Up-sell / Cross-sell</th>
                                        <th colspan="2" class="text-center1">Payment Reminder </th>
                                        <th colspan="2" class="text-center1">Servicing and Relationship Calls</th>
                                        <th colspan="2" class="text-center1">Total</th>
                                    </tr>
									<tr>
                                        
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>Tasks assigned this month</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">10</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks overdue</td>
                                        <td class="text-center1">4</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">9</td>
                                        <td class="text-center1">23%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Newly Assigned</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">10</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks created by RRO (MTD)</td>
                                        <td class="text-center1">6</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">11</td>
                                        <td class="text-center1">28%</td>
                                    </tr>
                                    <tr class="gradeD">
                                        <td>Total tasks for current month</td>
                                        <td class="text-center1">20</td>
                                        <td class="text-center1">9</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">11</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">40</td>
                                        <td class="text-center1">100%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Successful</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">8</td>
                                        <td class="text-center1">20%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Closed (Successful + Failed)</td>
                                        <td class="text-center1">4</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">56%</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">14</td>
                                        <td class="text-center1">35%</td>
                                    </tr>
                                    <tr class="gradez">
                                        <td>Total no. of activities</td>
                                        <td class="text-center1" colspan="2">10</td>
                                        <td class="text-center1" colspan="2">8</td>
                                        <td class="text-center1" colspan="2">10</td>
                                        <td class="text-center1" colspan="2">36</td>
                                     </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

        </div>
		<div class="col-lg-12 col-sm-12 col-md-12">
		<div class="row">
		<h4 class="heading-tip">Week-To-Date (WTD)</h4>
		  <div class="col-lg-6 col-sm-6 col-md-6">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Up-sell / Cross-sell</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_01.svg" class="img-t" alt="Up-sell-Cross-sell-r" height="320">
            </div>
			<div class="del-am">
			<span>No. Activities Today</span>
			<small>No. Activities WTD</small>
			<div class="clearfix"></div>
			</div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-6 col-sm-6 col-md-6">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Loan Maintenance</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_02.svg" class="img-t" alt="Loan Maintenance" height="320">
            </div>
			<div class="del-am">
			<span>No. Activities Today</span>
			<small>No. Activities WTD</small>
			<div class="clearfix"></div>
			</div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-6 col-sm-6 col-md-6">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Relationship Calls</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_03.svg" class="img-t" alt="Relationship Calls" height="320">
            </div>
			<div class="del-am">
			<span>No. Activities Today</span>
			<small>No. Activities WTD</small>
			<div class="clearfix"></div>
			</div>
		  </div>
		  </div>
		 </div>
		  <div class="col-lg-6 col-sm-6 col-md-6">
		  <div class="ibox float-e-margins">
		  <div class="ibox-title"><h5>Servicing and Relationship Calls</h5></div>
		  <div class="ibox-content">
		  <div class="rrochart">
           <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_04.svg" class="img-t" alt="Relationship Calls" height="320">
            </div>
			<div class="del-am">
			<span>No. Activities Today</span>
			<small>No. Activities WTD</small>
			<div class="clearfix"></div>
			</div>
		  </div>
		  </div>
		 </div>
		</div>
		</div>
    </div>