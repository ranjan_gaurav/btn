<?php

/*

 */

class Rroleader extends Rroleader_Controller {

    function __construct() {
        parent::__construct();
        //  error_reporting(0);
        $this->load->library("session");
        $this->load->model('rro_leader_model', 'rro_leader');
    }

    /*
     * RRM Leader Performance Dashboard
     */

    public function index($userID = '11') {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/demo/hunterCharts.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/custom.js',
            'assets/js/hunterCharts.js',
            'assets/js/user_profile.js',
            'assets/js/chnagemodal.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['meta_title'] = "Bank BRI";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['main_content'] = 'index';
        $data['info'] = $this->rro_leader->getHunterInfo();
        $data['result'] = $this->rro_leader->getHunterData();
        $users = array('first_name' => 'ANTON MARACI', 'user_pic' => '1.jpg', 'username' => 'anton');
        $data['users'] = (object) $users;
        //$data['users']=$this->rro_leader->GetUserInformation($userID);
        $this->setData($data);
    }

    /*
     * RRM Activity Pipeline
     */

    public function pipeline() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/pipeline.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/codemirror/codemirror.css',
            'assets/css/plugins/codemirror/ambiance.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Activity Pipeline";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['result'] = $this->rro_leader->getHunterList();
        $data['alerts'] = $this->rro_leader->getAlerts();
        $this->setData($data);
    }

    /*
     * RRM Leader Performance Dashboard : Team View
     */

    public function performance() {
        error_reporting(0);
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/jasny/jasny-bootstrap.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/daterangepicker/daterangepicker.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/performance.js',
            'assets/js/user_profile.js',
            'assets/js/chnagemodal.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/datapicker/datepicker3.css',
            'assets/css/plugins/daterangepicker/daterangepicker-bs3.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Performance Dashboard - Team View";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['result'] = $this->rro_leader->getRROList();
        $this->setData($data);
    }

    public function process() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/demo/hunterCharts.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/custom.js',
            'assets/js/hunterCharts.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['meta_title'] = "Bank BRI";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['main_content'] = 'index';
        $data['info'] = $this->rro_leader->getHunterInfo();
        $data['result'] = $this->rro_leader->getHunterData();
        $this->setData($data);
    }

    /*
     * RRM Leader Portfolio Summary
     */

    public function summary() {
        error_reporting(0);
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/barChart.jquery.js',
            'assets/js/user_profile.js',
            'assets/js/chnagemodal.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/barchart.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI RRM Summary";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['clients'] = $this->rro_leader->getClientList();
        $this->setData($data);
    }

    /*
     * Client Profile
     */

    public function profile($clientID = '4') {


        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/profile.js',
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/datapicker/datepicker3.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Client Profile";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['client'] = $this->rro_leader->getClientInfo($clientID);
        $data['clients'] = $this->rro_leader->getClientList();
        $data['client_id'] = $clientID;
        $this->setData($data);
    }

    /*
     * Opportunity Contest Dashboard - CASA Balance
     */

    public function contest_casa() {

        $val = $this->uri->segment('3');

        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/morris/morris.js',
            'assets/js/demo/morris-demo.js',
            'assets/js/plugins/morris/raphael-2.1.0.min.js',
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Contest Dashboard";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['client'] = $this->rro_leader->getClientInfo($val);

        //  print_r( $data['client']);
        //  exit;

        $this->setData($data);
    }

    /*
     * Opportunity Contest Dashboard - OD Balance
     */

    public function contest_od() {
        $val = $this->uri->segment('3');
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/morris/morris.js',
            'assets/js/demo/morris-demo.js',
            'assets/js/plugins/morris/raphael-2.1.0.min.js',
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Contest Dashboard";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['client'] = $this->rro_leader->getClientInfo($val);
        $this->setData($data);
    }

    /*
     * Opportunity Contest Dashboard - Online Banking
     */

    public function contest_online() {
        $val = $this->uri->segment('3');
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Contest Dashboard";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $data['client'] = $this->rro_leader->getClientInfo($val);
        $this->setData($data);
    }

    /*
     * Activity Planner
     */

    public function activity() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/jquery-ui.custom.min.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/plugins/fullcalendar/fullcalendar.min.js',
            'assets/js/plugins/jasny/jasny-bootstrap.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/clockpicker/clockpicker.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/datetimepicker.js',
            'assets/js/activity.js',
        );
        $includeCss = array(
            'assets/css/datetimepicker.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/fullcalendar/fullcalendar.css',
            'assets/css/plugins/fullcalendar/fullcalendar.print.css',
            'assets/css/plugins/clockpicker/clockpicker.css',
            'assets/css/plugins/steps/jquery.steps.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        // $this->form_validation->set_rules('purpose', 'purpose', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {

            $insert = $this->rro_leader->insertActivity();
            if ($insert) {
                $this->session->set_flashdata('message', 'Activity has been created successfully');
            } else {
                $data['error'] = 'Something wrong Happen with database;';
            }
        }
        $data['result'] = $this->rro_leader->listActivity();
        $data['meta_title'] = "Bank BRI Activity";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    /*
     * Activity Report
     */

    public function report() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jasny/jasny-bootstrap.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/plugins/daterangepicker/daterangepicker.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/staps/jquery.steps.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/activity_report.js',
            'assets/js/bootstrap-datetimepicker.min.js',
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/datapicker/datepicker3.css',
            'assets/css/plugins/daterangepicker/daterangepicker-bs3.css',
            'assets/css/bootstrap-datetimepicker.min.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Activity Report";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Activity Name', 'required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $insert = $this->rro_leader->insertActivity();
            if ($insert) {
                $this->session->set_flashdata('message', 'Your Activity Has Been saved successfully.');
                redirect(base_url('activity'));
            } else {
                $data['error'] = 'Something wrong Happen with database;';
            }
        }
        $this->setData($data);
    }

    public function user_info() {
        $userID = $this->input->post('id');
        $data['users'] = $this->rro_leader->getUserDetails($userID);
        $this->load->view('profile_info', $data);
    }

    public function load() {
        error_reporting(0);

        $q = intval($_GET ['q']);
        if ($q == 5) {
            $this->load->view('processIndicatorpopup');
        }

        if ($q == 11) {
            $this->load->view('modalfinancialKPI');
        }

        if ($q == 2) {
            $data['clients'] = $this->rro_leader->getClientListing();
            $this->load->view('modalsummary', $data);
        }
    }

    public function refferals() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/referrals.js',
            'assets/js/refferalsdelete.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css'
        );

        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lead_name', 'Name', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data ['error'] = validation_errors();
        } else {
            $insert = $this->rro_leader->add_reffrals();
            if ($insert) {
                redirect(base_url('referrals/tracking'));
            }
        }
        $data['meta_title'] = "Bank BRI Refferals";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

}

?>